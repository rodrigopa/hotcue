<?php
# Framework
include 'framework/start.php';

require "admin/settings.php";

// Remover metatag generator
remove_action('wp_head', 'wp_generator');

// add scripts in page
function my_scripts() {
	// deregistering old jquery
	wp_deregister_script('jquery');

	// styles
	wp_register_style('fontawesome', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css');
    wp_enqueue_style('fontawesome' );

	wp_register_style('bootstrap', get_template_directory_uri() . '/assets/css/bootstrap.min.css');
    wp_enqueue_style('bootstrap' );

    wp_register_style('style', get_template_directory_uri() . '/style.css');
    wp_enqueue_style('style' );

	// scripts
	wp_register_script('jquery', get_stylesheet_directory_uri() . '/assets/js/jquery.min.js', null, null, true);
	wp_enqueue_script('jquery');
	
	wp_register_script('angularjs', get_stylesheet_directory_uri() . '/assets/js/angular.min.js', null, null, true);
	wp_enqueue_script('angularjs');

	wp_register_script('angularjs-ui-router', get_stylesheet_directory_uri() . '/assets/js/angular-ui-router.min.js', null, null, true);
	wp_enqueue_script('angularjs-ui-router');

	wp_register_script('app', get_stylesheet_directory_uri() . '/assets/js/app.js', null, null, true);
	wp_enqueue_script('app');

	wp_register_script('controllers', get_stylesheet_directory_uri() . '/assets/js/controllers.js', null, null, true);
	wp_enqueue_script('controllers');

	wp_register_script('bootstrap', get_stylesheet_directory_uri() . '/assets/js/bootstrap.min.js', null, null, true);
	wp_enqueue_script('bootstrap');

	wp_register_script('cycle2', get_stylesheet_directory_uri() . '/assets/js/jquery.cycle2.min.js', null, null, true);
	wp_enqueue_script('cycle2');

	wp_register_script('cycle2-carousel', get_stylesheet_directory_uri() . '/assets/js/jquery.cycle2.carousel.min.js', null, null, true);
	wp_enqueue_script('cycle2-carousel');

	wp_register_script('boxer', get_stylesheet_directory_uri() . '/assets/js/jquery.fs.boxer.min.js', null, null, true);
	wp_enqueue_script('boxer');

	wp_register_script('script', get_stylesheet_directory_uri() . '/assets/js/script.js', null, null, true);
	wp_enqueue_script('script');

	wp_localize_script( 'angularjs', 'Site', 
		array( 
			'url' => get_bloginfo('template_directory').'/', 
			'site' => get_bloginfo('wpurl'), 
			'templates' => trailingslashit( get_template_directory_uri() ) . 'templates/',
		)
	);
}

add_action( 'wp_enqueue_scripts', 'my_scripts' );
add_filter('show_admin_bar', '__return_false');

/** Add Custom Field To Category Form */
add_action( 'category_add_form_fields', 'category_form_custom_field_add', 10 );
add_action( 'category_edit_form_fields', 'category_form_custom_field_edit', 10, 2 );
 
function category_form_custom_field_add( $taxonomy ) {
?>
<div class="form-field">
  	<label for="category_color">Cor</label>
  	<input name="category_color" id="category_color" type="text" value="" size="40" aria-required="true" />
  	<p class="description">Digite uma cor</p>
</div>
<?php
}
 
function category_form_custom_field_edit( $tag, $taxonomy ) {
    $option_name = 'category_color_' . $tag->term_id;
    $category_color = get_option( $option_name );
 
?>
<tr class="form-field">
  	<th scope="row" valign="top"><label for="category_color">Cor</label></th>
  	<td>
    	<input type="text" name="category_color" id="category_color" value="<?php echo esc_attr( $category_color ) ? esc_attr( $category_color ) : ''; ?>" size="40" aria-required="true" />
    	<p class="description">Digite uma cor</p>
  	</td>
</tr>
<?php
}
 
/** Save Custom Field Of Category Form */
add_action( 'created_category', 'category_form_custom_field_save', 10, 2 ); 
add_action( 'edited_category', 'category_form_custom_field_save', 10, 2 );
 
function category_form_custom_field_save( $term_id, $tt_id ) {
    if ( isset( $_POST['category_color'] ) ) {           
        $option_name = 'category_color_' . $term_id;
        update_option( $option_name, $_POST['category_color'] );
    }
}

$general_fields = get_option( "general" );
$admin_email = get_option( "admin_email" );
