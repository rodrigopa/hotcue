<?php
session_start();

// Carregando opções
$vektor_options = require("config/options.php");
$GLOBALS["options"] = $vektor_options;

// Começando o IdiORM
require_once __DIR__ . "/vendor/Idiorm/Idiorm.php";
// Começando o Paris
require_once __DIR__ . "/vendor/Idiorm/Paris.php";

// Configurações da database
ORM::configure($vektor_options['db']);
ORM::configure('logging', true);

// Configuração do Twig
require_once __DIR__ . "/vendor/Twig/Autoloader.php";
Twig_Autoloader::register();
$loader = new Twig_Loader_Filesystem(__DIR__ . "/app/views");
$twig = new Twig_Environment($loader, array(
	'debug' => true
));
$twig->addExtension(new Twig_Extension_Debug());

// TwigFilters
$twig->addFilter( new Twig_SimpleFilter('pastTime', array('\App\Controllers\Util', 'pastTime')) );

// Instânciando o Fat Free Framework
$app = require __DIR__ . "/vendor/FatFree/base.php";

// Adicionando funções globais no Twig
$twig->addGlobal('app', Base::instance());
$flashs = array(
	"error" 	=> ( isset( $_SESSION["error"] ) 	? $_SESSION["error"] 	: null ),
	"success" 	=> ( isset( $_SESSION["success"] ) 	? $_SESSION["success"] 	: null )
);

$twig->addGlobal("session", $flashs);
$twig->addGlobal("base", $vektor_options["base"]);

// config
$app->set("AUTOLOAD", __DIR__ . "/");
$app->set('DEBUG', 2);
$app->set('vektor', $vektor_options);

// Criando as rotas
require_once "config/routes.php";
// Iniciando a aplicação
$app->run();
