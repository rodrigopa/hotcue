$(".boxer").boxer();

window.animateScroll = function() {
	var top = $( "#banner" ).offset().top + $( "#banner" ).outerHeight();
	var bodyPaddingTop = parseInt( $( "body" ).css("padding-top").replace("px", "") );
	top -= bodyPaddingTop;
	$("body, html").animate({ scrollTop: top }, 400);
}

window.sessionStorageJSON = {
	exists: function(key, id) {
		var list = this.getItem(key);
		if (!list) return false;

		for (var i = 0; i < list.length; i++) {
			var item = list[ i ];
			if (item.id == id) {
				return true;
			}
		}

		return false;
	},
	setItem: function(key, obj) {
    	return sessionStorage.setItem(key, JSON.stringify(obj));
	},
	getItem: function(key) {
		var data = sessionStorage.getItem(key);
		if (!data) return false;
	    return JSON.parse(data);
	},
	clear: function(key) {
		sessionStorage.setItem(key, "");
	}
}