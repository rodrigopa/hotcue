app

.factory("API", function( $rootScope, $http ) {
	var self = this;

	var url = $rootScope.url;
	var restRoutes = {
		news: {
			index: url + "api/news/page/",
			show: url + "api/news/"
		},
		agenda: {
			index: url + "api/agenda",
			show: url + "api/agenda/show",
		},
		artists: {
			index: url + "api/artists/type/",
			show: url + "api/artists/"
		},
		members: {
			index: url + "api/members/"
		},
		request: {
			store: url + "api/request/"
		},
		contact: {
			send: url + "api/contact"
		}
	};

	// news
	self.News = {
		index: function( page, callback ) {
			if (typeof page == "undefined" ) page = 1;
			$http.get(restRoutes.news.index + page).then(
				function(response) { callback(response.data); },
				function(response) { console.error("Não foi possível carregar as notícias!"); }
			);
		},
		show: function( id, callback ) {
			$http.get(restRoutes.news.show + id).then(
				function(response) { callback(response.data); },
				function(response) { console.error("Não foi possível carregar a notícia! ID: " + id); }
			);
		}
	};

	// agenda
	self.Agenda = {
		index: function( callback ) {
			$http.get(restRoutes.agenda.index).then(
				function(response) { callback(response.data); },
				function(response) { console.error("Não foi possível carregar a agenda!"); }
			);
		},
		show: function( query_string, callback ) {
			$http.get(restRoutes.agenda.show + query_string).then(
				function(response) { callback(response.data); },
				function(response) { console.error("Não foi possível carregar a agenda!"); }
			);
		}
	};

	// artist
	self.Artists = {
		index: function( type, callback ) {
			$http.get(restRoutes.artists.index + type).then(
				function(response) { callback(response.data); },
				function(response) { console.error("Não foi possível carregar os artistas!"); }
			);
		},
		show: function( id, callback ) {
			$http.get(restRoutes.artists.show + id).then(
				function(response) { callback(response.data); },
				function(response) { console.error("Não foi possível carregar o artista! ID: " + id); }
			);
		}
	};

	// members
	self.Members = {
		index: function( callback ) {
			$http.get(restRoutes.members.index).then(
				function(response) { callback(response.data); },
				function(response) { console.error("Não foi possível carregar os membros da equipe!"); }
			);
		}
	};

	// contact
	self.Contact = {
		send: function( fields, callback ) {
			$http({
				method: "POST",
				url: restRoutes.contact.send,
				data: $.param( fields ),
				headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
			}).then(
				function(response) { callback(response.data); },
				function(response) { console.error("Não foi possível enviar o email!"); }
			);
		}
	};

	// request
	self.Request = {
		store: function( fields, callback ) {
			$http({
				method: "POST",
				url: restRoutes.request.store,
				data: $.param( fields ),
				headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
			}).then(
				function(response) { callback(response.data); },
				function(response) { console.error("Não foi possível enviar a cotação!"); }
			);
		}
	}

	return this;
})

.factory("NewsListComponent", function( API ) {
	var self = this;

	self.page = 1;
	self.canPrev = false;
	self.canNext = false;
	self.end = true;

	var reload = function() {
		self.btnPrev = jQuery( "#arrow-hotnews-prev" );
		self.btnNext = jQuery( "#arrow-hotnews-next" );
		self.canPrev = !(self.page == 1);
		self.canNext = !(self.end);

		if (self.canPrev) {
			self.btnPrev.removeClass("disabled");
		} else {
			self.btnPrev.addClass("disabled");
		}

		if (self.canNext) {
			self.btnNext.removeClass("disabled");
		} else {
			self.btnNext.addClass("disabled");
		}
	}

	self.request = function(page, callback) {
		API.News.index(page, function(data) {
			self.page = page;
			self.end = data.end;
			reload();
			callback(data.rows);
		});
	}

	self.prev = function() {
		if (!self.canPrev) return;
		request(self.page-1);
	}

	self.next = function() {
		if (!self.canNext) return;
		request(self.page+1);
	}

	return self;
})

.factory("AgendaComponent", function( API, Loading, AlertMessage ) {
	var self = this;

	var resultsElement = null;
	self.artist = null;
	self.month = null;

	self.getQueryString = function() {
		var queryString = "";

		if ( self.artist ) {
			queryString += "artist=" + self.artist;
		}

		if ( self.month ) {
			if ( queryString !== "" ) {
				queryString += "&";
			}

			queryString += "month=" + self.month;
		}

		if ( queryString !== "" ) {
			queryString = "?" + queryString;
		}

		return queryString;
	}

	self.request = function(callback) {
		Loading.show();

		API.Agenda.index(function(data) {
			Loading.hide();
			callback(data);
		});
	}

	self.requestDates = function(callback) {
		Loading.show();

		API.Agenda.show(self.getQueryString(), function(data) {
			Loading.hide();
			callback(data);
		});
	}

	self.reloadDates = function(list) {
		var html = "";

		if (list && list.length > 0) {
			list.forEach(function(elem, index) {
				html += '<div class="date">\
				                <a target="_blank" href="' + elem.link + '">\
				                    <time><span class="day">' + elem.date.day + '</span><span class="month">/' + elem.date.monthStr + ' ' + elem.date.year + '</span></time>\
				                    <h2>' + elem.title + '</h2>\
				                    <p><i class="fa fa-map-marker"></i> ' + elem.address + '.</p>\
				                    <button class="btn">Informações <i class="fa fa-angle-right"></i></button>\
				                </a>\
				            </div>';
			});

			jQuery( "#agenda .dates .container" ).html( html );
		} else {
			AlertMessage.show();
		}
	}

	self.start = function(elem) {
		// setting elem
		resultsElement = elem;

		// start
		Loading.start( resultsElement );
		AlertMessage.start( resultsElement, "empty" );

		// request agenda
		self.request(function(data) {
			var artists = data.artists,
				dates = data.dates,
				months = data.months;

			// generate select months
			months.forEach(function(elem, index) {
				if (elem.selected) {
					self.month = elem.id
				}

				jQuery( "#agenda #select-month" ).append( '<option value="' + elem.id + '"' + (elem.selected ? ' selected' : '') + '>' + elem.name + '</option>' );
			});

			// add artists
			artists.forEach(function(elem, index) {
				jQuery( "#agenda .djs" ).append( '<li data-id="' + elem.id + '"><a href="javascript:;">' + elem.name + '</a></li>' );
			});

			// add dates
			self.reloadDates(dates);
		});

		// register click events
		jQuery( "body" ).on( "click", "#agenda .djs li", function() {
			var id = jQuery( this ).data( "id" );
			var $this = jQuery( this );
			var parent = jQuery( this ).parents( ".djs" ).first();

			// set artist id
			self.artist = id;

			// request agenda
			self.requestDates(function(data) {
				// add dates
				self.reloadDates(data.rows);

				// add selected class
				parent.find( "li a" ).removeClass( "selected" );
				$this.find( "a" ).addClass( "selected" );
			});
		});

		// register select events
		jQuery( "body" ).on( "change", "#agenda #select-month", function() {
			var value = jQuery( this ).val();
			
			// set month
			self.month = value;

			// request agenda
			self.requestDates(function(data) {
				// add dates
				self.reloadDates(data.rows);
			});
		});
	}

	return self;
})

.factory("Loading", function() {
	var self = this,
		createdElement = null,
		mainElem = null,
		html = '<div class="overlay-loading">\
					<div class="loading-ball">\
						<div></div>\
						<div></div>\
						<div></div>\
					</div>\
				</div>';

	self.start = function(elem) {
		mainElem = elem;
		createdElement = $( '<div></div>' ).html( html ).find( '.overlay-loading' );

		return self;
	}

	self.show = function() {
		jQuery( mainElem ).children( "*" ).hide();
		createdElement.appendTo( mainElem );
	}

	self.hide = function() {
		jQuery( mainElem ).children( "*" ).show();
		createdElement.remove();
	}

	return self;
})

.factory("AlertMessage", function() {
	var self = this,
		mainElem = null,
		createdElement = null,
		template = '<div class="alert-message-static">\
						<i class="fa fa-{{icon}}"></i>\
						<p>{{message}}</p>\
					</div>';

	var types = {
		empty: { icon: "ban", message: "Nenhum item encontrado!" },
		error: { icon: "exclamation-triangle", message: "Desculpe-nos, mas aconteceu algum erro.<br>Tente novamente!" },
		emailSuccess: { icon: "check", message: "Email enviado com sucesso!" },
		emailError: { icon: "exclamation-triangle", message: "Não foi possível enviar o email, tente novamente!" },
		newsletterSuccess: { icon: "check", message: "Email cadastrado com sucesso!" },
		newsletterError: { icon: "exclamation-triangle", message: "Não foi possível cadastrar o email, tente novamente!" },
		requestSuccess: { icon: "check", message: "Sua solicitação de cotação foi enviada com sucesso. <br>Em breve responderemos você!" },
		requestError: { icon: "exclamation-triangle", message: "Não foi possível enviar sua solicitação de cotação, tente novamente!" },
	};

	self.start = function( elem, type ) {
		mainElem = elem;
		var values = types[ type ];
		var html = template.replace( '{{icon}}', values.icon );
			html = html.replace( '{{message}}', values.message );

		createdElement = $( '<div></div>' ).html( html ).find( '.alert-message-static' );

		return self;
	}

	self.show = function() {
		jQuery( mainElem ).html( createdElement );
	}

	self.hide = function() {
		createdElement.remove();
	}

	return this;
})

.directive('dynFbCommentBox', function () {
    function createHTML(href, numposts) {
        return '<div class="fb-comments" ' +
                       'data-href="' + href + '" ' +
                       'data-numposts="' + numposts + '">' +
               '</div>';
    }

    return {
        restrict: 'A',
        scope: {},
        link: function postLink(scope, elem, attrs) {
            attrs.$observe('pageHref', function (newValue) {
                var href        = attrs.pageHref;// newValue;
                var numposts    = attrs.numposts    || 5;

                elem.html(createHTML(href, numposts));
                FB.XFBML.parse(elem[0]);
            });
        }
    };
})

.directive('fbPage', ['$window', '$rootScope', function($window, $rootScope) {
    return {
      restrict: 'A',
      scope: {},
      template: '<div class="fb-page" data-href="{{page}}" data-tabs="timeline" data-height="{{height}}" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"></div>',
      link: function($scope, $element, $attrs) {
        var requested = false;
        
        ['page','height'].forEach(function(name){
            $attrs.$observe('fb' + name.charAt(0).toUpperCase() + name.slice(1), function(val){
                $scope[name] = val;
            });
        });
        
        function request(){
            if (!requested) {
                requested = true;
                requestAnimationFrame(update);
            }
        }
        
        function update(){
            $scope.$evalAsync(function() {
                $scope.width = $element.parent().width();
                $scope.$applyAsync(function() {
                    FB.XFBML.parse($element[0], function(){
                        requested = false;
                    });
                });
            });
        }

        $window.addEventListener('resize', request);
        update();
      }
    };
}]);