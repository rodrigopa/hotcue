'use strict';

var app = angular.module('app', ['ui.router', 'ngSanitize']);

app.run(function($rootScope, Loading, AlertMessage, $http) {
    $rootScope.templatesDir = Site.templates;
    $rootScope.url = Site.url;
    // $rootScope.site = BlogInfo.site;
    // $rootScope.site = BlogInfo.site;
    
    // loading animation
    Loading.start( "#site-content" );
    $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) { 
        Loading.show();
    });

    $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) { 
        Loading.hide();
        // animate scroll
        animateScroll();
    });

    // newsletter
    var getNewsletterFieldValue = function( field ) {
        return jQuery( "#form-newsletter .field-" + field ).val().trim();
    }

    var storeNewsletter = function() {}

    $rootScope.submitNewsletterForm = function() {
        var fields = {};
        fields.name = getNewsletterFieldValue( "nome" );
        fields.email = getNewsletterFieldValue( "email" );

        $http({
            method: "POST",
            url: $rootScope.url + "api/newsletter",
            data: $.param( fields ),
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
        }).then(
            function(response) {
                var data = response.data;

                if (data.error) {
                    AlertMessage.start( "#form-newsletter", "newsletterError" );
                    AlertMessage.show();
                } else {
                    AlertMessage.start( "#form-newsletter", "newsletterSuccess" );
                    AlertMessage.show();
                }
            },
            function(response) { console.error("Não foi possível cadastrar seu email!"); }
        );
    }
})

.config(function($urlRouterProvider, $stateProvider, $locationProvider) {
    $locationProvider.html5Mode(true);

    $urlRouterProvider.otherwise("/home");
    $stateProvider
        .state("home", {
            url: "/home",
            views: {
                "content": {
                    controller: "HomeController",
                    templateUrl: Site.templates + "home.html",
                }
            }
        })

        .state("newsShow", {
            url: "/news/:id",
            views: {
                "content": {
                    controller: "ShowNewsController",
                    templateUrl: Site.templates + "noticia.html"
                }
            }
        })

        .state("agenda", {
            url: "/agenda",
            views: {
                "content": {
                    controller: "AgendaController",
                    templateUrl: Site.templates + "agenda.html",
                }
            }
        })

        .state("artistasNacionais", {
            url: "/artistas-nacionais",
            views: {
                "content": {
                    controller: "ArtistasNacionaisController",
                    templateUrl: Site.templates + "artistas.html"
                }
            }
        })

        .state("artistasInternacionais", {
            url: "/artistas-internacionais",
            views: {
                "content": {
                    controller: "ArtistasInternacionaisController",
                    templateUrl: Site.templates + "artistas.html"
                }
            }
        })

        .state("artistaShow", {
            url: "/artista/:id",
            params: {
                fromRequest: false
            },
            views: {
                "content": {
                    controller: "ShowArtistaController",
                    templateUrl: Site.templates + "artista.html"
                }
            }
        })

        .state("news", {
            url: "/news",
            views: {
                "content": {
                    controller: "IndexNewsController",
                    templateUrl: Site.templates + "hotnews.html"
                }
            }
        })

        .state("equipe", {
            url: "/equipe",
            views: {
                "content": {
                    controller: "EquipeController",
                    templateUrl: Site.templates + "equipe.html"
                }
            }
        })

        .state("solicitacao", {
            url: "/solicitacao",
            params: {
                lastArtist: null,
                duplicate: false
            },
            views: {
                "content": {
                    controller: "SelectController",
                    templateUrl: Site.templates + "select.html"
                }
            }
        })

        .state("contato", {
            url: "/contato",
            views: {
                "content": {
                    controller: "ContatoController",
                    templateUrl: Site.templates + "contato.html"
                }
            }
        })
})