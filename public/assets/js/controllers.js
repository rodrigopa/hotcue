app

.controller("HomeController", function($scope, NewsListComponent, AgendaComponent) {
	// start with an empty array
	$scope.news = [];

	// request news
	NewsListComponent.request(1, function(rows) {
		$scope.news = rows;
	});

	// start agenda
	AgendaComponent.start( "#agenda .dates .container" );
})

.controller("AgendaController", function(AgendaComponent) {
	// start agenda
	AgendaComponent.start( "#agenda .dates .container" );
})

.controller("IndexNewsController", function($scope, NewsListComponent) {
	// start with an empty array
	$scope.news = [];

	// request news
	NewsListComponent.request(1, function(rows) {
		$scope.news = rows;
	});
})

.controller("ShowNewsController", function($scope, $stateParams, API) {
	$scope.item = {};
	var id = parseInt( ($stateParams.id).split("-")[ 0 ] );
	$scope.getLocation = function() {
		return "http://www.hotcue.com.br";// document.location.href;
	}

	// request news item
	API.News.show(id, function(data) {
		$scope.item = data;
	});
})

.controller("ArtistasNacionaisController", function($scope, API, Loading) {
	$scope.type = "nacionais";
	$scope.artists = [];

	Loading.start( ".template-artistas" );
	Loading.show();
	API.Artists.index($scope.type, function(data) {
		Loading.hide();
		$scope.artists = data;
	});
})

.controller("ArtistasInternacionaisController", function($scope, API, Loading) {
	$scope.type = "internacionais";
	$scope.artists = [];

	Loading.start( ".template-artistas" );
	Loading.show();
	API.Artists.index($scope.type, function(data) {
		Loading.hide();
		$scope.artists = data;
	});
})

.controller("EquipeController", function($scope, API, Loading) {
	$scope.members = [];

	Loading.start( "#equipe .template-artistas" );
	Loading.show();
	API.Members.index(function(data) {
		Loading.hide();
		$scope.members = data;
	});
})

.controller("ShowArtistaController", function($scope, $timeout, $sce, $state, $stateParams, API, Loading) {
	$scope.item = {};
	$scope.btnRequestTitle = "Solicitar pedido";
	var id = parseInt( ($stateParams.id).split("-")[ 0 ] );

	// change the button title
	if ($stateParams.fromRequest === true) {
		$scope.btnRequestTitle = "Adicionar artista";
	}

	$scope.getYoutubeIframeSrc = function (videoId) {
  		return $sce.trustAsResourceUrl('https://www.youtube.com/embed/' + videoId);
	};

	$scope.getSoundCloudIframeSrc = function(videoUrl) {
		return $sce.trustAsResourceUrl( videoUrl );
	}

	$scope.request = function() {
		var selectedArtists = sessionStorageJSON.getItem("artistsSelected");
		if ( !(selectedArtists instanceof Array) ) {
			selectedArtists = [];
		}

		var param = {};
		if (!sessionStorageJSON.exists("artistsSelected", $scope.item.id)) {
			var item = {};
			item.id = $scope.item.id;
			item.name = $scope.item.name;
			item.image = $scope.item.image;
			item.image_black = $scope.item.image_black;
			item.place = $scope.item.place;

			selectedArtists.push( item );
			sessionStorageJSON.setItem("artistsSelected", selectedArtists);
			param.lastArtist = item;
		} else {
			param.duplicate = true;
		}

		$state.go("solicitacao", param);
	}

	Loading.start( '.artista .container' );
	Loading.show();
	API.Artists.show(id, function(data) {
		Loading.hide();
		$scope.item = data;

		if ( data.pictures.length > 0 ) {
			// set pictures
			$timeout(function() {
				jQuery( "#fotos #cycle-pictures" ).cycle();
				jQuery( ".boxer" ).boxer( "destroy" );
			}, 1);
		}
	});
})

.controller("ContatoController", function($scope, API, Loading, AlertMessage) {
	Loading.start( "#form-contato" );

	var getFieldValue = function( field ) {
		return jQuery( "#form-contato .field-" + field ).val().trim();
	}

	$scope.send = function() {
		var fields = {};
		fields.nome = getFieldValue( "nome" );
		fields.email = getFieldValue( "email" );
		fields.telefone = getFieldValue( "telefone" );
		fields.assunto = getFieldValue( "assunto" );
		fields.mensagem = getFieldValue( "mensagem" );

		Loading.show();
		API.Contact.send( fields, function( data ) {
			Loading.hide();

			if (data.error) {
				AlertMessage.start( "#form-contato", "emailError" );
				AlertMessage.show();
			} else {
				AlertMessage.start( "#form-contato", "emailSuccess" );
				AlertMessage.show();
			}
		});
	}
})

.controller("SelectController", function($scope, $state, $stateParams, API, Loading, AlertMessage) {
	$scope.selected = [];
	$scope.availablesArtistsNacional = [];
	$scope.availablesArtistsInternacional = [];

	var messages = {
		last 		: "Você selecionou o artista <strong>{{artista}}</strong>, deseja selecionar mais algum artista?",
		duplicate	: "Ops, você já selecionou este artista. Deseja selecionar outro?"
	};
	var selectedArtists = sessionStorageJSON.getItem("artistsSelected");
	var lastArtist = $stateParams.lastArtist;

	// check duplicate artist
	if ( $stateParams.duplicate === true ) {
		var message = messages.duplicate;
		jQuery( "#selected .message" ).removeClass("hidden").find(".text-message").html( message );
	}

	// check if has passed a last artist param
	if ( lastArtist instanceof Object ) {
		var message = messages.last;
		message = message.replace('{{artista}}', lastArtist.name);
		jQuery( "#selected .message" ).removeClass("hidden").find(".text-message").html( message );
	}

	// get the session selected artists list
	if ( selectedArtists instanceof Array ) {
		$scope.selected = selectedArtists;
	}

	$scope.getArtists = function() {
		jQuery( "#selected .message" ).addClass( "hidden" );
		jQuery( "#more" ).removeClass( "hidden" );
		jQuery( "#selected .end-button-container" ).removeClass( "hidden" );
		Loading.start( "#more" );
		Loading.show();
		API.Artists.index("", function(data) {
			Loading.hide();

			for (var i = 0; i < data.length; i++) {
				var elem = data[ i ];
				if (elem.type == "nacional") {
					$scope.availablesArtistsNacional.push( elem );
				} else {
					$scope.availablesArtistsInternacional.push( elem );
				}
			};
		});
	}

	$scope.removeArtist = function(item) {
		for (var i = 0; i < $scope.selected.length; i++) {
			if ($scope.selected[ i ].id == item.id) {
				$scope.selected.splice(i, 1);
				sessionStorageJSON.setItem("artistsSelected", $scope.selected);
			}
		}
	}

	$scope.done = function() {
		jQuery( "#selected" ).addClass( "end" );
		jQuery( "#selected .message" ).addClass("hidden");
		jQuery( "#selected .end-button-container" ).addClass( "hidden" );
	}

	$scope.addArtist = function( item ) {
		if (!sessionStorageJSON.exists("artistsSelected", item.id)) {
			$scope.selected.push( item );
			sessionStorageJSON.setItem("artistsSelected", $scope.selected);
			animateScroll();
		}
	}

	var getFieldValue = function( field ) {
		return jQuery( "#form-select .field-" + field ).val().trim();
	}

	var getSelectedArtistsAsString = function() {
		var selectedString = "";
		for (var i = 0; i < $scope.selected.length; i++) {
			selectedString += $scope.selected[ i ].name + "\n";
		}

		return selectedString;
	}

	$scope.submitRequest = function() {
		var fields = {};
		fields.nome = getFieldValue( "nome" );
		fields.email = getFieldValue( "email" );
		fields.telefone = getFieldValue( "telefone" );
		fields.localizacao = getFieldValue( "localizacao" );
		fields.mensagem = getFieldValue( "mensagem" );
		fields.artists = getSelectedArtistsAsString();

		Loading.start( "#form-select" );
		Loading.show();
		API.Request.store(fields, function(data) {
			Loading.hide();

			if (data.error) {
                AlertMessage.start( "#form-select", "requestError" );
                AlertMessage.show();
            } else {
                AlertMessage.start( "#form-select", "requestSuccess" );
                AlertMessage.show();
                sessionStorageJSON.clear("artistsSelected");
            }
		});
	}

	$scope.goToProfile = function( url ) {
		$state.go("artistaShow", { id: url, fromRequest: true });
	}

	$scope.cancelRequest = function() {
		jQuery( "#selected" ).removeClass( "end" );
		jQuery( "#selected .end-button-container" ).removeClass( "hidden" );
	}
});