<?php
session_start();

// Carregando opções
$vektor_options = require("config/options.php");
$GLOBALS["options"] = $vektor_options;

// Começando o IdiORM
require_once __DIR__ . "/vendor/Idiorm/Idiorm.php";

// Configurações da database
ORM::configure($vektor_options['db']);
ORM::configure('logging', true);

// Configuração do Twig
require_once __DIR__ . "/vendor/Twig/Autoloader.php";
Twig_Autoloader::register();
$loader = new Twig_Loader_Filesystem( array(__DIR__ . "/App/Views", __DIR__ . "/App/Packages") );
$twig = new Twig_Environment($loader, array(
	'debug' => true
));
$twig->addExtension(new Twig_Extension_Debug());

// Instânciando o Fat Free Framework
$app = require __DIR__ . "/vendor/FatFree/base.php";

// Adicionando funções globais no Twig
$twig->addGlobal('app', Base::instance());
$flashs = array(
	"error" 		=> ( isset( $_SESSION["error"] ) 		? $_SESSION["error"] 		: null ),
	"success" 		=> ( isset( $_SESSION["success"] ) 		? $_SESSION["success"] 		: null ),
	"after_action" 	=> ( isset( $_SESSION["after_action"] ) ? $_SESSION["after_action"] : null ),
);

$twig->addGlobal("session", $flashs);
$twig->addGlobal("base", $vektor_options["base"]);
$twig->addGlobal("upload_dir", $vektor_options["upload_dir"]);
$twig->addGlobal("placeholder_empty", $vektor_options["placeholder_empty"]);

// config
$app->set("AUTOLOAD", __DIR__ . "/");
$app->set('DEBUG', 3);
$app->set('vektor', $vektor_options);

// Criando as rotas
require_once "config/routes.php";
// Iniciando a aplicação
$app->run();
