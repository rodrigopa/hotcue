<?php
namespace App\Models;

class Vektor {

	public function getAllMenu() {
		// $sql_menu = "SELECT menu_nome, menu_id, menu_icone FROM " . $GLOBALS["options"]["prefix"] . "menu WHERE menu_status='Ativo' ORDER BY menu_ordem";
		return \ORM::for_table( $GLOBALS["options"]["prefix"] . "menu" )
				->where( "menu_status", "Ativo" )
				->order_by_asc( "menu_ordem" )
				->find_many();
	}

	public function getModulesByMenu( $id_menu ) {
		$sql = "SELECT DISTINCT(" . $GLOBALS["options"]["prefix"] . "tabelas.tab_id), " . $GLOBALS["options"]["prefix"] . "tabelas.tab_nome, " . $GLOBALS["options"]["prefix"] . "tabelas.tab_target, " . $GLOBALS["options"]["prefix"] . "tabelas.tab_link 
					FROM " . $GLOBALS["options"]["prefix"] . "tabelas, " . $GLOBALS["options"]["prefix"] . "tipo_permissao, " . $GLOBALS["options"]["prefix"] . "user_tipo_rel
					WHERE " . $GLOBALS["options"]["prefix"] . "user_tipo_rel.tp_usr_id = " . $GLOBALS["options"]["prefix"] . "tipo_permissao.tp_usr_id 
					AND " . $GLOBALS["options"]["prefix"] . "tipo_permissao.tab_id = " . $GLOBALS["options"]["prefix"] . "tabelas.tab_id
					AND " . $GLOBALS["options"]["prefix"] . "tabelas.tab_status='Ativo' 
					AND " . $GLOBALS["options"]["prefix"] . "tabelas.menu_id='{$id_menu}' 
					AND " . $GLOBALS["options"]["prefix"] . "user_tipo_rel.usr_id = '" . $_SESSION[ $GLOBALS[ "options" ]["session_name"] ][ "id" ] . "'
					ORDER BY " . $GLOBALS["options"]["prefix"] . "tabelas.tab_ordem";

		return \ORM::for_table( $GLOBALS[ "options" ]["prefix"] . "menu" )
				->raw_query( $sql )
				->find_many();
	}

	public function checkUserStatus() {
		$erro = null;

		// Checando status atual do usuário
		$count_status_user = \ORM::for_table( $GLOBALS["options"]["prefix"] . "users" )
						->where( "id", $_SESSION[ $GLOBALS[ "options" ]["session_name"] ][ "id" ] )
						->where( "status", "Ativo" )
						->count();

		if ( $count_status_user == 0 ) {
			return "Seu usuário foi desativado!";
		}

		// Checando se o usuário foi banido
		$ban = \ORM::for_table( $GLOBALS["options"]["prefix"] . "users_ban" )
						->where( "usr_id", $_SESSION[ $GLOBALS[ "options" ]["session_name"] ][ "id" ] )
						->where_gte( "data_fim", time() )
						->find_one();

		if ( $ban !== false ) {
			$data_fim = date('d/m/Y H:i', $ban->data_fim);
			return "Você foi banido até <strong>{$data_fim}</strong>!<br>Motivo: {$ban->motivo}.";
		}

		return false;
	}

	public function getBestRole() {
		$sql = "SELECT tp_usr_nome FROM {$GLOBALS["options"]["prefix"]}user_tipo ut INNER JOIN {$GLOBALS["options"]["prefix"]}user_tipo_rel utr ON utr.tp_usr_id = ut.tp_usr_id
				WHERE utr.usr_id = :id
				ORDER BY ut.tp_ordem ASC";

		$query = \ORM::for_table( $GLOBALS["options"]["prefix"] . "user_tipo" )
				->raw_query( $sql, array( "id" => $_SESSION[ $GLOBALS[ "options" ]["session_name"] ][ "id" ] ) )
				->find_one();

		if ( $query )
			return $query->tp_usr_nome;

		return null;
	}

	public function addToLog($tab_id, $record_id, $url, $verb, $ip, $browser, $os) {
		try {
			$query = \ORM::for_table( $GLOBALS["options"]["prefix"] . "log" )->create();

			$query->tab_id       	= $tab_id;
			$query->usr_id       	= $_SESSION[ $GLOBALS[ "options" ]["session_name"] ][ "id" ];
			$query->log_registro 	= $record_id;
			$query->log_time     	= time();
			$query->log_url      	= $url;
			$query->log_action   	= $verb;
			$query->log_ip       	= $ip;
			$query->log_browser  	= $browser;
			$query->log_os       	= $os;

			$query->save();
		} catch (\PDOException $e) {}
	}

}