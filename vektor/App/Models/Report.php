<?php
namespace App\Models;

class Report {
	protected $table = "avisos";

	public function getAll() {
		$sql = "SELECT DISTINCT(a.avi_id), a.* FROM " . $GLOBALS[ "options" ]["prefix"] . "avisos a 
			LEFT JOIN " . $GLOBALS[ "options" ]["prefix"] . "avisos_usuario au ON a.avi_id=au.avi_id
			LEFT JOIN " . $GLOBALS[ "options" ]["prefix"] . "avisos_user_tipo aus ON a.avi_id=aus.avi_id
			LEFT JOIN " . $GLOBALS[ "options" ]["prefix"] . "user_tipo_rel usr ON usr.tp_usr_id=aus.tp_usr_id
			WHERE avi_status='1' AND (usr.usr_id='" . $_SESSION[ $GLOBALS[ "options" ]["session_name"] ][ "id" ] . "' OR au.user_id='" . $_SESSION[ $GLOBALS[ "options" ]["session_name"] ][ "id" ] . "')
			ORDER BY avi_data DESC";

		return \ORM::for_table( $GLOBALS[ "options" ]["prefix"] . $this->table )
						->raw_query( $sql )
						->find_many();
	}
}
