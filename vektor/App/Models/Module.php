<?php
namespace App\Models;
use App\Controllers\Util as Util;

class Module {
	/**
	 * ID da tabela
	 */
	public $id_tabela = null;

	/**
	 * Tipo de visualização
	 */
	private $view_type = null;

	/**
	 * Error
	 */
	public $error = false;

	/**
	 * Error code
	 */
	public $error_code = 404;

	/**
	 * Error message
	 */
	public $error_message = null;

	/**
	 * Search query
	 */
	public $search_query = null;

	/**
	 * Informações da tabela
	 */
	public $table = array();

	/**
	 * Paramêtros
	 */
	public $parameters = array();

	/**
	 * Relações
	 */
	public $relations = array();

	/**
	 * Permissão
	 */
	public $permission = array(
		"edita"     	=> true,
		"insere"    	=> true,
		"deleta"    	=> true,
		"view" 			=> true
	);

	/**
	 * Pegar campo chave primária da tabela
	 */
	public $primary_key = null;

	/**
	 * Todos os campos da tabela
	 */
	public $fields = array();

	/**
	 * Entidade pai
	 */
	public $parent = null;

	/**
	 * Critério de ordenação
	 */
	public $ordem = array();

	/**
	 * show route
	 */
	private $show = false;


	public function __construct( $id_tabela, $view_type, $show = false )
	{
		$this->id_tabela = $id_tabela;
		$this->view_type = $view_type;
		$this->show = $show;

		// Pegar dados da tabela
		$this->getTableData();
		// Se houver algum erro ao pegar os dados da tabela, já interrompemos a execução
		if ($this->error === true) return;
	}

	public function getParameters()
	{
		$parameters = \ORM::for_table( $GLOBALS[ "options" ][ "prefix" ] . "parametros" )
						->left_outer_join( $GLOBALS[ "options" ][ "prefix" ] . "parametro_tipo", 
							array( $GLOBALS[ "options" ][ "prefix" ] . "parametro_tipo.param_tp_id", "=", $GLOBALS[ "options" ][ "prefix" ] . "parametros.param_tp_id" ) 
						)->where( "tab_id", $this->id_tabela )
						->where( "param_status", "Ativo" )
						->where_not_null( "param_{$this->getViewTypeLayout()}" )
						->order_by_asc( "param_ordem" )
						->find_many();

		foreach ($parameters as $parameter) {
			$this->parameters[ $parameter->param_campo ] = $parameter;
		}
	}

	private function getTableData()
	{
		$table_data = \ORM::for_table( $GLOBALS[ "options" ][ "prefix" ] . "tabelas" )
						->join( $GLOBALS[ "options" ][ "prefix" ] . "menu",
							array( $GLOBALS[ "options" ][ "prefix" ] . "tabelas.menu_id", "=", $GLOBALS[ "options" ][ "prefix" ] . "menu.menu_id" ) 
						)
						->where( "tab_id", $this->id_tabela )
						->where( "tab_status", "Ativo" )
						->where_null( "tab_{$this->view_type}" )
						->find_one();

		if ( $table_data === false ) {
			$this->error = true;
		} else {
			$this->table = $table_data;
		}
	}

	private function getViewTypeLayout()
	{
		if ($this->show) return "form";
		if ($this->view_type == "view") {
			return "lista";
		} else if ($this->view_type == "insere" || $this->view_type == "edita") {
			return "form";
		}

		return null;
	}

	public function getRelations() {
		$this->relations = \ORM::for_table( $GLOBALS[ "options" ][ "prefix" ] . "per_relacionado" )
			->join( $GLOBALS[ "options" ][ "prefix" ] . "tabelas", 
				array( $GLOBALS[ "options" ][ "prefix" ] . "per_relacionado.pr_id_filho", "=", $GLOBALS[ "options" ][ "prefix" ] . "tabelas.tab_id" ) 
			)->where( "pr_id_pai", $this->id_tabela )
			->group_by('pr_id_filho')
			->find_many();
	}

	public function getForeignKeyRelations() {
		$this->foreign_keys_relations = \ORM::for_table( $GLOBALS[ "options" ][ "prefix" ] . "per_relacionado" )
			->join( $GLOBALS[ "options" ][ "prefix" ] . "tabelas", 
				array( $GLOBALS[ "options" ][ "prefix" ] . "per_relacionado.pr_id_pai", "=", $GLOBALS[ "options" ][ "prefix" ] . "tabelas.tab_id" ) 
			)
			->where( "pr_id_filho", $this->id_tabela )
			->find_many();
	}

	private function getForeignKeyRelationsByColumn( $column ) {
		return \ORM::for_table( $GLOBALS[ "options" ][ "prefix" ] . "per_relacionado" )
			->join( $GLOBALS[ "options" ][ "prefix" ] . "tabelas", 
				array( $GLOBALS[ "options" ][ "prefix" ] . "per_relacionado.pr_id_pai", "=", $GLOBALS[ "options" ][ "prefix" ] . "tabelas.tab_id" ) 
			)
			->where( "pr_id_filho", $this->id_tabela )
			->where( "pr_campo_foreign", $column )
			->group_by('pr_id_filho')
			->group_by('pr_id_pai')
			->find_one();
	}

	public function getCount()
	{
		$query = \ORM::for_table( $this->table->tab_tabela );
		$query = $this->filterSimpleSearch( $query );
		$query = $this->filterDataWithRelation( $query );
		$query = $this->filterLogged( $query );
		$query = $this->filterDataWithParent( $query );

		return $query->count();
	}

	public function getData( $current_page, $limit )
	{
		try {
			$query = \ORM::for_table( $this->table->tab_tabela )
						->select( "{$this->table->tab_tabela}.*" );
			$query = $this->filterSimpleSearch( $query );
			$query = $this->filterDataWithRelation( $query );
			$query = $this->filterLogged( $query );
			$query = $this->filterDataWithParent( $query );
			$query = $this->setOrderCriteria( $query );

			$query = $query
					->limit( $limit )
					->offset( ($limit * $current_page) - $limit )
					->find_many();
			
			return $query;
		} catch (\PDOException $e) {
			var_dump( $e );
			$this->error = true;
			$this->error_message = "Erro ao listar registros! {$e->getMessage()}";

			return false;
		}
	}

	public function getPermissions()
	{
		$tipoPermissions = $this->getTableTipoPermissions( $this->table->tab_id );

		if ($tipoPermissions) {
			// Permissão de edição
			if ( !is_null( $this->table->tab_edita ) || $tipoPermissions->tab_edita !== "Sim" ) {
				$this->permission["edita"] = false;
			}

			// Permissão de inseção
			if ( !is_null( $this->table->tab_insere ) || $tipoPermissions->tab_insere !== "Sim" ) {
				$this->permission["insere"] = false;
			}

			// Permissão de deletar
			if ( !is_null( $this->table->tab_deleta ) || $tipoPermissions->tab_deleta !== "Sim" ) {
				$this->permission["deleta"] = false;
			}

			// Permissão de visualizar
			if ( !is_null( $this->table->tab_view ) || $tipoPermissions->tab_view !== "Sim" ) {
				$this->permission["view"] = false;
			}

			// checar permissão de visualização baseada no tipo
			if ( $this->permission[ $this->view_type ] === false ) {
				throw new \Exception("Você não tem permissão!", 401);
			}
		} else {
			throw new \Exception("Você não tem permissão!", 401);
		}
	}

	private function checkPermission()
	{
		if ( $this->permission[ $this->view_type ] === false ) {
			$this->error = true;
			$this->error_code = 401;
		}
	}

	public function getTableTipoPermissions( $tab_id ) {
		$sql = "select tp.* from {$GLOBALS["options"]["prefix"]}tipo_permissao tp
				inner join {$GLOBALS["options"]["prefix"]}user_tipo ut ON tp.tp_usr_id = ut.tp_usr_id
				inner join {$GLOBALS["options"]["prefix"]}user_tipo_rel rel ON tp.tp_usr_id = rel.tp_usr_id
				where
					rel.usr_id = '" . $_SESSION[ $GLOBALS[ "options" ]["session_name"] ][ "id" ] . "' and
					tp.tab_id = {$tab_id}";

		return \ORM::for_table( $GLOBALS[ "options" ][ "prefix" ] . "tipo_permissao" )
				->raw_query( $sql )
				->find_one();
	}

	private function setOrderCriteria( $query )
	{
		if ( isset( $_GET[ "ordem" ] ) && is_array( $_GET[ "ordem" ] ) ) {
			$this->ordem = $_GET[ "ordem" ];
		} else {
			$this->ordem[ "direcao" ] = "asc";

			foreach ($this->fields as $field) {
				// Checando a existência de um paramêtro do tipo ordem
				if ( $field["parameter"]->param_tp_nome == "ordem" ) {
					$this->ordem[ "campo" ] = $field["parameter"]->param_campo;
					$this->ordem[ "direcao" ] = "asc";
					break;
				} else if ( in_array( $field["parameter"]->param_tp_nome, array( "data_cad", "data", "data_alt", "data_hora" ) ) ) {
					$this->ordem[ "campo" ] = $field["parameter"]->param_campo;
					$this->ordem[ "direcao" ] = "desc";
				}
			}

			if ( !isset( $this->ordem[ "campo" ] ) ) {
				$this->ordem[ "campo" ] = $this->table->tab_campo;
			}
		}

		if ($this->ordem["direcao"] == "asc") {
			$query = $query->order_by_asc( $this->ordem[ "campo" ] );
		} else {
			$query = $query->order_by_desc( $this->ordem[ "campo" ] );
		}
		
		return $query;
	}

	public function getFields($viewType = "list")
	{
		$dbh = new \PDO( $GLOBALS["options"]["db"]["connection_string"], 
							$GLOBALS["options"]["db"]["username"], 
							$GLOBALS["options"]["db"]["password"] );

		$interrogacoes = null;
		if ( count( $this->parameters ) > 0 ) {
			$interrogacoes = str_repeat("?, ", count( $this->parameters ) - 1) . "?";
		}
		$q = $dbh->prepare("SHOW COLUMNS FROM {$this->table->tab_tabela} WHERE Field IN ({$interrogacoes})");

		$i = 0;
		foreach ($this->parameters as $nameField => $parameter) {
			$q->bindValue(1 + $i, $nameField, \PDO::PARAM_STR);
			$i++;
		}

		$q->execute();
		$table_fields = $q->fetchAll();
		$dbh = null;

		$fields = array();
		foreach ($table_fields as $field) { $fields[ $field["Field"] ] = $field; }

		foreach ($this->parameters as $nameField => $parameter) {
			$parameterName = $parameter->param_tp_nome;
			
			if ( !is_null( $parameterName ) && !file_exists( $GLOBALS["options"]["root"] . "/App/Views/parameter/{$parameterName}/_{$viewType}.html" ) ) continue;

			$field = $fields[ $nameField ];
			$infoColumn = Util::getColumnSize( $field["Type"] );

			$this->fields[ $field["Field"] ] = array(
				"type" 		=> $infoColumn["type"],
				"size" 		=> $infoColumn["size"],
				"required" 	=> $field["Null"] == "NO",
				"default" 	=> $field["Default"],
				"unique" 	=> ( $field["Key"] == "UNI" ),
				"element"   => Util::getElementByType( $infoColumn["type"] ),
				"parameter" => $parameter,
				"relation"  => $this->getForeignKeyRelationsByColumn( $field[ "Field" ] ),
				"enum_values" => Util::getEnumValues( $infoColumn[ "type" ] )
			);
		}
	}

	public function getPrimaryKey()
	{
		$primary_key = $this->returnPrimaryKey( $this->table->tab_tabela );

		if ($primary_key !== false) {
			$this->primary_key = $primary_key;

			\ORM::configure('id_column_overrides',
				array( $this->table->tab_tabela => $primary_key )
			);
		} else {
			$this->error = true;
			$this->error_code = 500;
			$this->error_message = "A tabela tem que possuir uma chave primária.";
		}
	}

	private function returnPrimaryKey( $table )
	{
		$sql = "SHOW KEYS FROM {$table} WHERE Key_name = 'PRIMARY'";

		$chave_primaria = \ORM::for_table( $table )
						->raw_query( $sql )
						->find_one();

		if ( $chave_primaria !== false )
		{
			return $chave_primaria->Column_name;
		}

		return false;
	}

	public function deleteAll( Array $records )
	{
		return \ORM::for_table( $this->table->tab_tabela )
				->where_id_in( $records )
				->delete_many();
	}

	public function getMaxOrder( $field ) {
		$sql = "SELECT MAX({$field}) AS value FROM {$this->table->tab_tabela}";
		$query = \ORM::for_table( $this->table->tab_tabela )
				->raw_query( $sql )
				->find_one();
		$maxOrder = (int) $query->value;

		return $maxOrder;
	}

	private function filterSimpleSearch( $query )
	{
		if ( !is_null( $this->search_query ) ) {
			$where_args = array();
			$where_operations = array();


			foreach ($this->parameters as $parameter) {
				$where_args[] = array( "{$this->table->tab_tabela}.{$parameter->param_campo}" => "%{$this->search_query}%" );
				$where_operations[ "{$this->table->tab_tabela}.{$parameter->param_campo}" ] = "LIKE";
			}

			$query->where_any_is( $where_args, $where_operations );
		}

		return $query;
	}

	private function filterDataWithRelation( $query )
	{
		$this->getForeignKeyRelations();
		if ($this->foreign_keys_relations === false || count($this->foreign_keys_relations) == 0) return $query;

		$index = 0;
		foreach ($this->foreign_keys_relations as $foreign_key) {
			$foreign_primary_key = $this->returnPrimaryKey( $foreign_key->tab_tabela );

			$query = $query->select( "table_alias_{$foreign_key->pr_campo_foreign}.{$foreign_key->tab_campo}", "foreign_field_{$foreign_key->pr_campo_foreign}" );
			$query = $query->left_outer_join(
				$foreign_key->tab_tabela,
				array( "table_alias_{$foreign_key->pr_campo_foreign}.{$foreign_primary_key}", "=", "{$this->table->tab_tabela}.{$foreign_key->pr_campo_foreign}" ),
				"table_alias_{$foreign_key->pr_campo_foreign}"
			);

			$index++;
		}

		return $query;
	}

	public function create( $values ) {
		$this->error = false;
		$query = \ORM::for_table( $this->table->tab_tabela )->create();

		foreach ($values as $field => $value) {
			$query->$field = $value;
		}

		try {
			$query->save();
		} catch (\PDOException $e) {
			$this->error = true;
			$this->error_message = $e->getMessage();
		}
	}

	public function update( $id_record, $values ) {
		$this->error = false;
		$query = \ORM::for_table( $this->table->tab_tabela )->find_one( $id_record );

		foreach ($values as $field => $value) {
			$query->$field = $value;
		}

		try {
			$query->save();
		} catch (\PDOException $e) {
			$this->error = true;
			$this->error_message = $e->getMessage();
		}
	}

	public function getDataToEdit( $id_record ) {
		$query = \ORM::for_table( $this->table->tab_tabela );
		$query = $this->filterDataWithRelation( $query );


		foreach ($this->fields as $fieldName => $field) {
			$query = $query->select( "{$this->table->tab_tabela}.{$fieldName}" );
		}

		$query = $query->where( $this->primary_key, $id_record )
			->find_one();

		return $query;
	}

	private function filterLogged( $query )
	{
		if ($this->table->tab_user == "Sim") {
			$query->where( "usr_id", $_SESSION[ $GLOBALS[ "options" ][ "session_name"] ][ "id" ] );
		}

		return $query;
	}

	private function filterDataWithParent( $query ) {
		if ( isset( $_GET[ "parent" ][ "id" ] ) && isset( $_GET[ "parent" ][ "value" ] ) ) {
			$id = $_GET[ "parent" ][ "id" ];
			$value = $_GET[ "parent" ][ "value" ];

			// Verificando existência de parentesco
			// 
			foreach ($this->foreign_keys_relations as $relation) {
				if ($relation->pr_id_pai == $id) {
					$foreign_primary_key = $this->returnPrimaryKey( $relation->tab_tabela );
					$query = $query->where( "table_alias_{$relation->pr_campo_foreign}.{$foreign_primary_key}", $value );
					break;
				}
			}
		}

		return $query;
	}

	public function swapOrder( array $ids, $field ) {
		$sql = "UPDATE
				{$this->table->tab_tabela} AS table1
				JOIN {$this->table->tab_tabela} AS table2 ON
				( table1.{$this->primary_key} = :id_one AND table2.{$this->primary_key} = :id_two )
				SET
				table1.{$field} = table2.{$field},
				table2.{$field} = table1.{$field}";

		$params = array(
			"id_one" => $ids[ 0 ],
			"id_two" => $ids[ 1 ]
		);

		return \ORM::raw_execute( $sql, $params );
	}

}
