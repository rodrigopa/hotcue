<?php
namespace App\Controllers;
use App\Models\Module as Module;

class AccountController extends ModuleController {
    private $table_id = null;

    private function setAccountModuleId() {
        $user = \ORM::for_table( $GLOBALS[ "options" ][ "prefix" ] . "tabelas" )
                        ->where( "tab_tabela", "vektor_users" )
                        ->find_one();

        $this->table_id = $user->tab_id;
    }

    public function index( $f3, $params ) {
        // search for user data
        $this->setAccountModuleId();
        
        $view_type = "edita";
        $this->model = new Module( $this->table_id, $view_type );

        // Verificar a existência de algum erro
        if ($this->model->error) {
            $this->error( $this->model->error_code );
        }

        try {
            // Pegar chave primária
            $this->model->getPrimaryKey();

            // Pegar parâmetros da tabela
            $this->model->getParameters();

            // Pegar relacionamentos da tabela
            $this->model->getRelations();

            // Pegar dados dos campos
            $this->model->getFields( "form" );
        } catch (\Exception $e) {
            $this->app->set("SESSION.error", "Você não possui permissão para essa ação!");
            $this->app->reroute( $params[0] );
        }

        /* Retorno dos dados */
        foreach ($this->model->parameters as $key => $parameter) {
            if ( is_null( $parameter->param_tp_nome ) ) {
                $this->model->parameters[ $key ]->param_tp_nome = "default";
            }
        }

        $data = array();

        if ($view_type == "edita" || $view_type == "view")
        {
            $id_record = $_SESSION[ $GLOBALS[ "options" ]["session_name"] ][ "id" ];
            $data = $this->model->getDataToEdit( $id_record );

            if ($data === false) {
                $this->error( $this->model->error_code );
                exit;
            }
        }

        $vars = array(
            "title"             => "Meus dados",
            "action"            => $view_type == "insere" ? "Inserir" : "Editar",
            "params"            => $params,

            "module"            => $this->model->table,
            "fields"            => $this->model->fields,
            "relations"         => $this->model->relations,
            "permissions"       => $this->model->permission,
            "primary_key"       => $this->model->primary_key,
            "record_data"       => $data
        );

        // retornando a view
        echo $this->twig->render("account/_form.html", $vars);
    }

    public function update( $f3, $params ) {
         // search for user data
        $this->setAccountModuleId();
        
        $view_type = "edita";
        $this->model = new Module( $this->table_id, $view_type );

        $id_record = $_SESSION[ $GLOBALS[ "options" ]["session_name"] ][ "id" ];
        $params[ "id_record" ] = $id_record;

        // Verificar a existência de algum erro
        if ($this->model->error) {
            $this->error( $this->model->error_code );
        }

        try {
            // Pegar chave primária
            $this->model->getPrimaryKey();
            
            // Pegar parâmetros da tabela
            $this->model->getParameters();

            // Pegar relacionamentos da tabela
            $this->model->getRelations();

            // Pegar dados dos campos
            $this->model->getFields();
        } catch (\Exception $e) {
            $this->app->set("SESSION.error", "Você não possui permissão para essa ação!");
            $this->app->reroute( $params[0] );
        }

        $error = false;
        $error_message = null;
        $values = array();

        $i = 0;
        foreach ($this->model->fields as $fieldCampo => $field) {
            if (!isset($_POST[ "field_{$field["parameter"]->param_campo}" ]))
            {
                $param_name = $field[ "parameter" ]->param_tp_nome;
                if ( $param_name == "toggle_status" ) {
                    $_POST[ "field_{$field["parameter"]->param_campo}" ] = "0";
                } else if ( $param_name == "ordem") {
                    // pegar ordem maior e incrementar
                    $maxOrder = $this->model->getMaxOrder( $field["parameter"]->param_campo );
                    $_POST[ "field_{$field["parameter"]->param_campo}" ] = $maxOrder+1;
                } else {
                    continue;
                }
            }

            if ( is_null( $field["parameter"]->param_tp_nome ) ) {
                $parameterName = "default";
            } else {
                $parameterName = $field["parameter"]->param_tp_nome;
            }

            $methodName = "store";
            if ($view_type == "edita") {
                $methodName = "update";
            }

            // Verificando existência da classe do parâmetro
            $className = "App\Controllers\Parameter\\" . Util::titleCase( $parameterName ) . "Parameter";
            if ( !class_exists( $className ) ) { continue; }
            
            $parameterInstance = new $className;

            $parameterInstance->args = array(
                "field"     => $field,
                "index"     => $i,
                "value"     => $_POST[ "field_{$field["parameter"]->param_campo}" ],
                "module"    => $this->model->table,
                "params"    => $params,
                "method"    => $methodName,
                "primary_key" => $this->model->primary_key
            );
            
            $value = $parameterInstance->$methodName();
            if ($value === false) continue;

            $values[ $field["parameter"]->param_campo ] = $value;

            if ( count( $parameterInstance->errors ) > 0 ) {
                $error = true;
                $error_message = implode( "<br>", $parameterInstance->errors );
                break;
            }

            $i++;
        }

        $model = $this->model;
        $app = $this->app;
        // Sucesso
        $redirectWithSuccess = function() use ($app) {
            $route = "/account";
            $app->reroute( $route );
        };

        // Erro
        $redirectWithError = function() use ($app) {
            // header("Location: {$_SERVER['HTTP_REFERER']}");
            $app->reroute( $_SERVER['HTTP_REFERER'] );
        };

        if (!$error)
        {
            $id_record = $id_record;
            $this->model->update( $id_record, $values );

            if ($this->model->error) {
                $this->app->set("SESSION.error", "Erro ao editar registro! {$this->model->error_message}");
                $redirectWithError();
            } else {
                $this->app->set("SESSION.success", "Registro editado com sucesso!");
                $redirectWithSuccess();
            }
        } else {
            // Mostrar erros
            $this->app->set("SESSION.error", $error_message);
            $redirectWithError();
        }
    }
}
