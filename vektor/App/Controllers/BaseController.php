<?php
namespace App\Controllers;
use App\Models\Vektor as Vektor;

class BaseController {
    /**
     * Instância do objeto da aplicação Fat Free
     * @var FatFree
     */
    protected $app;

    /**
     * TWIG
     */
    protected $twig;

    /**
     * MODEL
     */
    protected $model;

    /**
     * VEKTOR MODEL
     */
    protected $vektorModel;

    /**
     * USER
     */
    protected $session;

    public function beforeRoute()
    {
        // check login
        $this->checkLogin();

        $this->session->user->role = $this->vektorModel->getBestRole();

        // Adicionando funções globais no Twig
        $this->twig->addGlobal("user_session", $this->session->user);

        // send menu to view
        $this->twig->addGlobal("menu", $this->getMenu());

        // Adicipnando para os logs
        if ( $this->app->VERB != "GET" ) {
            $id_module = null;
            $id_record = null;

            if ( isset( $this->app->PARAMS[ "id_module" ] ) ) $id_module = $this->app->PARAMS[ "id_module" ];
            if ( isset( $this->app->PARAMS[ "id_record" ] ) ) $id_record = $this->app->PARAMS[ "id_record" ];

            $uri        = $this->app->URI;
            $action     = $this->app->VERB;
            $ip         = $_SERVER[ "REMOTE_ADDR" ];
            $osystem    = Util::getOS();
            $browser    = Util::getBrowser();

            $this->vektorModel->addToLog( $id_module, $id_record, $uri, $action, $ip, $browser, $osystem );
        }
    }

    /**
     * Método que é chamado antes de executar o método mapeado na rota
     * @return null
     */
    public function checkLogin()
    {
        // verificar sessão
        $this->session = new SessionController($this->app);

        if (!$this->session->isLogged()) {
            $this->app->reroute('/login');
        }

        $checkUserStatus = $this->vektorModel->checkUserStatus();

        if ( $checkUserStatus !== false ) {
            $this->app->set("SESSION.error", $checkUserStatus);
            $this->app->reroute("/logout");
        }

        // verificar permissão
    }

    public function clearFlash()
    {
        // Limpando as flash messages
        $this->app->set("SESSION.error", null);
        $this->app->set("SESSION.success", null);
    }

    /**
     * Método construtor
     * @param FatFree $app Instância do objeto da aplicação
     */
    public function __construct()
    {
        global $twig;
        $this->twig = &$twig;
        $this->app = \Base::instance();
        $this->vektorModel = new Vektor;
    }

    private function getMenu() {
        $menus = $this->vektorModel->getAllMenu();
        $returnArray = array();

        foreach ($menus as $menu)
        {
            $modules = $this->vektorModel->getModulesByMenu( $menu->menu_id );

            if ( count( $modules ) > 0 ) {
                $menuArray = array( "data" => $menu, "modules" => $modules );
                $returnArray[] = $menuArray;
            }
        }
        
        return $returnArray;
    }

    protected function error() {
        $this->app->reroute("/public/404.php");
        exit;
    }
}
