<?php
namespace App\Controllers\Parameter;

class SenhaParameter extends DefaultParameter {

	/**
	 * Inserção dos dados no banco
	 */
	public function store() {
		$field = $this->args["field"];
		$this->filters( $field );

		return $this->args["value"];
	}

	/**
	 * Edição dos dados no banco
	 */
	public function update() {
		$field = $this->args["field"];

		if ( !empty( $this->args["value"] ) ) {
			$this->filters( $field );

			return $this->args["value"];
		}

		return false;
	}

}
