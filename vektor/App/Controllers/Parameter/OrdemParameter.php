<?php
namespace App\Controllers\Parameter;

class OrdemParameter extends BaseParameter {

	/**
	 * Inserção dos dados no banco
	 */
	public function store() {
		$field = $this->args["field"];
		$this->filters( $field );

		return $this->args["value"];
	}

	public function update() {
		return false;
	}
	
}
