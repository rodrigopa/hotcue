<?php
namespace App\Controllers\Parameter;

class ArquivoParameter extends BaseParameter {

	protected $allowed_extensions;
	protected $file = null;
	protected $action_type;

	public function __construct() {
		$this->allowed_extensions = $GLOBALS[ "options" ][ "allow_type_files" ];
	}

	public function filters( $field ) {
		$this->action_type = strtolower( $_POST[ "action_type" ] );

		if ( isset($_FILES[ "file_field_{$field["parameter"]->param_campo}" ]) ) {
			$this->file = $_FILES[ "file_field_{$field["parameter"]->param_campo}" ];
		}

		if ($field["required"]) {
			if ( !is_null( $this->file ) ) {
				if (!is_null( $this->allowed_extensions ) && !in_array( $this->getExtension( $this->file[ "name" ] ), $this->allowed_extensions )) {
					$this->errors[] = "O campo {$field["parameter"]->param_form} possui um arquivo inválido - Os válidos são: <strong><i>" . implode(", ", $this->allowed_extensions) . "</i></strong>!";
				}
			} else {
				$this->errors[] = "O campo {$field["parameter"]->param_form} é obrigatório!";
			}
		}
	}

	public function store() {
		$field = $this->args["field"];
		$this->filters( $field );

		return $this->upload();
	}

	public function update() {
		return $this->store();
	}

	public function upload() {
		if ( isset( $this->file ) && $this->file[ "error" ] !== 0 ) return false;
		$newFilePath = date("Y") . "/" . date("m");
		$upload_path = $GLOBALS["options"]["root"] . $GLOBALS["options"]["upload_dir"] . "/" . $newFilePath;

		if ( !file_exists( $upload_path ) ) {
			mkdir($upload_path, 0777, true);
		}

		$newFileName = md5( uniqid() . time() . $this->file[ "name" ] ) . "." . $this->getExtension( $this->file[ "name" ] );
		move_uploaded_file($this->file[ "tmp_name" ], $upload_path . "/" . $newFileName);

		// remover arquivo anterior
		if ($this->action_type == "editar") {
			if ( !empty($this->args[ "value" ]) ) {
				$pathInfo = pathinfo($this->args[ "value" ]);

				if (preg_match('/^[0-9]{4}\/[0-9]{2}$/', $pathInfo["dirname"]) === 1) {
					@unlink( $GLOBALS["options"]["root"] . $GLOBALS["options"]["upload_dir"] . "/" . str_replace( "../", "", $this->args[ "value" ] ) );
				}
			}
		}

		return $newFilePath . "/" . $newFileName;
	}

	protected function getExtension( $fileName ) {
		$fileNameParts = explode(".", $fileName);
		return strtolower( end($fileNameParts) );
	}

}
