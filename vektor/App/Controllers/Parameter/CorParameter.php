<?php
namespace App\Controllers\Parameter;

class CorParameter extends DefaultParameter {

	public function filters( $field ) {
		parent::filters( $field );

		if (!empty($this->args["value"])) {
			if (preg_match('/^#([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$/', $this->args["value"]) !== 1) {
    			$this->errors[] = "O campo <u>{$field["parameter"]->param_form}</u> não é uma cor válida!";
			}
		}
	}
	
}
