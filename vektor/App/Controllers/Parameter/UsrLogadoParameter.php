<?php
namespace App\Controllers\Parameter;

class UsrLogadoParameter extends BaseParameter {

	public function store() {
		return $_SESSION[ $GLOBALS[ "options" ][ "session_name" ] ][ "id" ];
	}

	public function update() { return false; }
	
}
