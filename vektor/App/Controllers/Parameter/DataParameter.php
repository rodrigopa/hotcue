<?php
namespace App\Controllers\Parameter;

class DataParameter extends BaseParameter {

	public function store() {
		$field = $this->args["field"];
		$returnDate = null;

		$newDataFormat = preg_replace('/([0-9]+)\/([0-9]+)\/([0-9]+)/', '$3-$2-$1 00:00:00', $this->args["value"] );

		if (in_array( $field[ "type" ], array( "int", "bigint", "timestamp" ) )) {
			$returnDate = strtotime( $newDataFormat );
		} else {
			$returnDate = $newDataFormat;
		}

		// Y-M-d h:i:s
		return $returnDate;
	}

}
