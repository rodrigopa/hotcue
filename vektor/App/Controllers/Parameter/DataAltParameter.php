<?php
namespace App\Controllers\Parameter;

class DataAltParameter extends BaseParameter {

	public function store() {
		$field = $this->args["field"];
		$returnDate = null;

		$newDataFormat = date( "Y/m/d H:i:s" );

		if (in_array( $field[ "type" ], array( "int", "bigint", "timestamp" ) )) {
			$returnDate = strtotime( $newDataFormat );
		} else {
			$returnDate = $newDataFormat;
		}

		// Y-M-d h:i:s
		return $returnDate;
	}

}
