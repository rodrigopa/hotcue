<?php
namespace App\Controllers\Parameter;

class UrlParameter extends DefaultParameter {

	public function filters( $field ) {
		parent::filters( $field );

		if (!empty($this->args["value"])) {
			if (!filter_var($this->args["value"], FILTER_VALIDATE_URL)) {
    			$this->errors[] = "O campo <u>{$field["parameter"]->param_form}</u> não é uma URL válida!";
			}
		}
	}
	
}
