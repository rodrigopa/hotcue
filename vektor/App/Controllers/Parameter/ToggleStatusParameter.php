<?php
namespace App\Controllers\Parameter;

class ToggleStatusParameter extends DefaultParameter {

	public function filters( $field ) {
		// Verificando obrigatoriedade do campo
		if ($field["required"]) {
			if ( !in_array( $this->args["value"], array( 0, 1 ) ) ) {
				$this->errors[] = "O campo {$field["parameter"]->param_form} é obrigatório!";
			}
		}
	}

}
