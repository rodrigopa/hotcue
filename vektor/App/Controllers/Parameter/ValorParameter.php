<?php
namespace App\Controllers\Parameter;

class ValorParameter extends DefaultParameter {

	public function store() {
		$this->filters( $this->args["field"] );
		return $this->moeda($this->args[ "value" ]);
	}

	private function moeda($get_valor) {
		$source = array('.', ','); 
		$replace = array('', '.');
		$valor = str_replace($source, $replace, $get_valor); //remove os pontos e substitui a virgula pelo ponto
		return $valor; //retorna o valor formatado para gravar no banco
	}
}
