<?php
namespace App\Controllers\Parameter;

class CnpjParameter extends DefaultParameter {

	public function filters( $field ) {
		parent::filters( $field );

		if (!empty($this->args["value"])) {
			$validateCpf = new \App\Controllers\Validate\ValidaCPFCNPJ( $this->args["value"] );

			if (!$validateCpf->valida_cnpj()) {
				$this->errors[] = "O campo <u>{$field["parameter"]->param_form}</u> não é um CNPJ válido!";
			}
		}
	}

}