<?php
namespace App\Controllers\Parameter;

class EmailParameter extends DefaultParameter {

	public function filters( $field ) {
		parent::filters( $field );

		if (!empty($this->args["value"])) {
			// Checar se email é válido
			if (!filter_var($this->args["value"], FILTER_VALIDATE_EMAIL)) {
    			$this->errors[] = "O campo <u>{$field["parameter"]->param_form}</u> não é um email válido!";
			}
		}
	}
	
}
