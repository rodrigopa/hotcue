<?php
namespace App\Controllers\Parameter;

class DefaultParameter extends BaseParameter {

	/**
	 * Inserção dos dados no banco
	 */
	public function store() {
		$field = $this->args["field"];
		$this->filters( $field );

		return $this->args["value"];
	}
	
}
