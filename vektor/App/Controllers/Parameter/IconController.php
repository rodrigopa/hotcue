<?php
namespace App\Controllers\Parameter;

class IconController extends \App\Controllers\BaseController {
	public function popup() {
		// retornando a view
		$vars = [];

        echo $this->twig->render("parameter/icon/popup.html", $vars);
	}
}
