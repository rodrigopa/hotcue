<?php
namespace App\Controllers\Parameter;

abstract class BaseParameter {
	/**
	 * Argumentos
	 */
	public $args = array();

	/**
	 * Errors
	 */
	public $errors = array();

	public function filters( $field ) {
		// Verificando obrigatoriedade do campo
		if ($field["required"]) {
			if (empty($this->args["value"])) {
				$this->errors[] = "O campo {$field["parameter"]->param_form} é obrigatório!";
			}
		} else {
			if (empty($this->args["value"])) {
				$this->args["value"] = null;
			}
		}
		
		// Verificar unicidade do campo
		if($field["unique"]) {
			if ($this->args["method"] == "store") {
				if ($this->checkRecordExists( $this->args["module"]->tab_tabela, $field["parameter"]->param_campo, $this->args["value"] )) {
					$this->errors[] = "Já existe um registro com este {$field["parameter"]->param_form}!";;
				}
			} else {
				if ($this->checkRecordExistsOnUpdate( $this->args["module"]->tab_tabela, $field["parameter"]->param_campo, $this->args["value"], $this->args[ "params" ][ "id_record" ] )) {
					$this->errors[] = "Já existe um registro com este {$field["parameter"]->param_form}!";;
				}
			}
		}

		// Verificando o tamanho do campo
		if (!is_null($field["size"]) && is_int($field["size"])) {
			if (strlen($this->args["value"]) > $field["size"]) {
				$this->args["value"] = substr($this->args["value"], 0, $field["size"]);
			}
		}
	}

	public function update() {
		return $this->store();
	}

	private function checkRecordExists( $table, $field, $value ) {
		return \ORM::for_table( $table )
				->where( $field, $value )
				->count() > 0;
	}

	private function checkRecordExistsOnUpdate( $table, $field, $value, $id_record ) {
		return \ORM::for_table( $table )
				->where( $field, $value )
				->where_not_equal( $this->args[ "primary_key" ], $id_record )
				->count() > 0;
	}

	abstract public function store();
}
