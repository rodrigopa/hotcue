<?php
namespace App\Controllers;
use App\Models\Module as Module;

class ModuleController extends BaseController {
    /**
     * Arquivo
     */
    protected $_arquivo = false;

    /**
     * Página atual
     */
    protected $_current_page = 1;

    /**
     * Registros por página
     */
    protected $_regs_per_page = 20;

    /**
     * MODEL
     */
    protected $model;

    public function afterRoute() {
        parent::clearFlash();
        $this->app->set("SESSION.after_action", null);
    }

    public function index( $f3, $params ) {
        $this->_list( $f3, $params, "module/_list.html" );
    }

    public function popup( $f3, $params ) {
        $this->_list( $f3, $params, "parameter/chave/popup.html" );
    }

    public function create( $f3, $params ) {
        $this->_form( $f3, $params, "insere" );
    }

    public function show( $f3, $params ) {
        $this->_form( $f3, $params, "view", true );
    }

    public function edit( $f3, $params ) {
        $this->_form( $f3, $params, "edita" );
    }

    public function store( $f3, $params ) {
        $args = array(
            "name_action"   => "inserir",
            "past_action"   => "inserido",
            "rest_path"     => "create"
        );
        $this->_save( $f3, $params, "insere", $args );
    }

    public function update( $f3, $params ) {
        $args = array(
            "name_action"   => "editar",
            "past_action"   => "editado",
            "rest_path"     => (int) $params[ "id_record" ] . "/edit"
        );
        $this->_save( $f3, $params, "edita", $args );
    }

    public function destroy( $f3, $params ) {
        $this->model = new Module( (int) $params[ "id_module" ], "deleta" );

        // Verificar a existência de algum erro
        if ($this->model->error) {
            $this->app->reroute( $params[0] );
        }

        try {
            // Pegar chave primária
            $this->model->getPrimaryKey();
            // Pegar permissões para ações
            $this->model->getPermissions();
        } catch (\Exception $e) {
            $this->app->set("SESSION.error", "Você não possui permissão para essa ação!");
            $this->app->reroute( $params[0] );
        }

        if ( isset( $_POST[ "del_key" ] ) && is_array( $_POST[ "del_key" ] ) ) {
            $del_key = $_POST[ "del_key" ];

            $model = $this->model;
            $app = $this->app;

            // closure delete all
            $deleteAll = function() use ($model, $app, $params, $del_key) {
                if ($model->deleteAll( $del_key ) ) {
                    $app->set("SESSION.success", "Os registros foram excluídos com sucesso!");
                } else {
                    $app->set("SESSION.error", "Não foi possível deletar os registros, tente novamente mais tarde!");
                }

                $app->reroute( $params[0] );
                exit;
            };

            if ( isset( $GLOBALS[ "options" ][ "max_size_delete" ] ) && !is_null( $GLOBALS[ "options" ][ "max_size_delete" ] ) ) {
                $max_size_delete = $GLOBALS[ "options" ][ "max_size_delete" ];

                if ( count( $del_key ) > $max_size_delete ) {
                    $this->app->set("SESSION.error", "Você só pode deletar até {$max_size_delete} registros de uma só vez!");
                    $this->app->reroute( $params[0] );
                    exit;
                } else {
                    $deleteAll();
                }
            } else {
                $deleteAll();
            }
        }

        $app->reroute( $params[0] );
    }

    public function postSearch() {}
    public function printView() {}

    public function checkPackageModule() {
        // verificar se o módulo é do tipo arquivo
        if ( !is_null($this->model->table->tab_arquivo) ) {
            $file = $GLOBALS[ "options" ][ "root" ] . "/app/packages/{$this->model->table->tab_arquivo}";

            if (!file_exists( $file )) {
                throw new \Exception("Arquivo não foi encontrado!");
            } else {
                $module = $this;
                require $file;
                exit;
            }
        }
    }

    private function _list( $f3, $params, $view_file ) {
        $this->model = new Module( (int) $params[ "id_module" ], "view" );

        // Verificar a existência de algum erro
        if ($this->model->error) {
            $this->error( $this->model->error_code );
        }

        try {
            $this->checkPackageModule();
            // Pegar chave primária
            $this->model->getPrimaryKey();

            // Pegar permissões para ações
            $this->model->getPermissions();

            // Pegar parâmetros da tabela
            $this->model->getParameters();

            // Pegar relacionamentos da tabela
            $this->model->getRelations();

            // Pegar dados dos campos
            $this->model->getFields();

            /* Busca simples */
            if ( isset($_GET[ "search_query"]) && !empty($_GET[ "search_query" ]) ) {
                $this->model->search_query = $_GET[ "search_query"];
            }

            /* Paginação */
            $total_records = $this->model->getCount();
            $total_pages = ceil( $total_records / $this->_regs_per_page );

            if ( isset( $params[ "page" ] ) ) {
                $this->_current_page = $params[ "page" ];
            }

            if ( $this->_current_page <= 0 || $this->_current_page > $total_pages ) {
                $this->_current_page = 1;
            }

            /* Retorno dos dados */
            foreach ($this->model->parameters as $key => $parameter) {
                if ( is_null( $parameter->param_tp_nome ) ) {
                    $this->model->parameters[ $key ]->param_tp_nome = "default";
                }
            }

            $data = $this->model->getData( $this->_current_page, $this->_regs_per_page );

            if ($data === false) {
                // Verificar a existência de algum erro
                //$this->error( null );
            }

            /* Query strings */

            $query_string_parent    = isset( $_GET["parent"] ) ? urldecode( http_build_query( array_intersect_key( $_GET, array( "parent" => null ) ) ) ) : null;
            $query_string_ordem     = isset( $_GET["ordem"] ) ? urldecode( http_build_query( array_intersect_key( $_GET, array( "ordem" => null ) ) ) ) : null;
            $query_string_all       = "";

            if (!is_null($query_string_ordem))  $query_string_all .= "?" . $query_string_ordem;
            if (!is_null($query_string_parent)) $query_string_all .= ($query_string_all == "" ? "?" : "&") . $query_string_parent;

            $vars = array(
                "action"            => "Listagem",

                "total_records"     => $total_records,
                "total_pages"       => $total_pages,
                "current_page"      => $this->_current_page,
                "regs_per_page"     => $this->_regs_per_page,
                "total_records_page"=> count( $data ),

                "module"            => $this->model->table,
                "fields"            => $this->model->fields,
                "data"              => $data,
                "relations"         => $this->model->relations,
                "permissions"       => $this->model->permission,
                "primary_key"       => $this->model->primary_key,
                "search_query"      => $this->model->search_query,
                "ordem"             => $this->model->ordem,
                "query_string"      => array(
                    "all"       => $query_string_all,
                    "parent"    => $query_string_parent,
                    "ordem"     => $query_string_ordem
                )
            );

            // retornando a view
            echo $this->twig->render($view_file, $vars);
        } catch (\Exception $e) {
            var_dump( $e );
            exit;
            $this->error( $e->getCode() );
        }
    }

    public function _form( $f3, $params, $view_type, $show = false ) {
        $this->model = new Module( (int) $params[ "id_module" ], $view_type, $show );

        // Verificar a existência de algum erro
        if ($this->model->error) {
            $this->error( $this->model->error_code );
        }

        try {
            // Pegar chave primária
            $this->model->getPrimaryKey();

            // Pegar parâmetros da tabela
            $this->model->getParameters();

            // Pegar relacionamentos da tabela
            $this->model->getRelations();

            // Pegar permissões para ações
            $this->model->getPermissions();

            // Pegar dados dos campos
            $this->model->getFields( "form" );

            /* Retorno dos dados */
            foreach ($this->model->parameters as $key => $parameter) {
                if ( is_null( $parameter->param_tp_nome ) ) {
                    $this->model->parameters[ $key ]->param_tp_nome = "default";
                }
            }

            $data = array();

            if ($view_type == "edita" || $view_type == "view")
            {
                $id_record = (int) $params[ "id_record" ];
                $data = $this->model->getDataToEdit( $id_record );

                if ($data === false) {
                    $this->error( $this->model->error_code );
                    exit;
                }
            }

            $vars = array(
                "action"            => $view_type == "insere" ? "Inserir" : "Editar",
                "params"            => $params,

                "module"            => $this->model->table,
                "fields"            => $this->model->fields,
                "relations"         => $this->model->relations,
                "permissions"       => $this->model->permission,
                "primary_key"       => $this->model->primary_key,
                "record_data"       => $data
            );

            $viewName = "_form";

            if ($view_type == "view") {
                $viewName = "_view";
            } else if ( $view_type == "search" ) {
                $viewName = "_search";
            }

            // retornando a view
            echo $this->twig->render("module/{$viewName}.html", $vars);
        } catch (\Exception $e) {
            $this->error( $e->getCode() );
        }
    }

    private function _save( $f3, $params, $view_type, $args ) {
        $list = false;

        if ( $_POST[ "action_type" ] == "editar_lista" ) {
            $list = true;
        }

        $this->model = new Module( (int) $params[ "id_module" ], $view_type );

        // Verificar a existência de algum erro
        if ($this->model->error) {
            $this->error( $this->model->error_code );
        }

        try {
            // Pegar chave primária
            $this->model->getPrimaryKey();

            // Pegar permissões para ações
            $this->model->getPermissions();

            // Pegar parâmetros da tabela
            $this->model->getParameters();

            // Pegar relacionamentos da tabela
            $this->model->getRelations();

            // Pegar dados dos campos
            $this->model->getFields();
        } catch (\Exception $e) {
            $this->app->set("SESSION.error", "Você não possui permissão para essa ação!");
            $this->app->reroute( $params[0] );
        }

        $error = false;
        $error_message = null;
        $values = array();

        if ( isset( $_POST[ "param_type" ] ) && $_POST[ "param_type" ] == "order_toggle" ) {
            $order_field = null;
            // verificar campo do tipo ordem
            foreach ($this->model->fields as $fieldCampo => $field) {
                if ( $field[ "parameter" ]->param_tp_nome == "ordem") {
                    $order_field = $field["parameter"]->param_campo;
                    break;
                }
            }

            // pegar dados e fazer a alteração
            $id_swap = $_POST[ "id_swap" ];

            if ( !is_null( $order_field ) && isset( $_POST[ "id_swap" ] ) ) {
                try {
                    $ids = array( $_POST[ "id_record" ], $_POST[ "id_swap" ] );
                    $this->model->swapOrder( $ids, $order_field );
                } catch (\PDOException $e) {
                    $error = true;
                    $error_message = $e->getMessage();
                }
            } else {
                $error = true;
                $error_message = "Erro ao trocar ordem, verifique se o tipo de dados do campo de ordem é int.";
            }
        } else {
            $i = 0;
            foreach ($this->model->fields as $fieldCampo => $field) {
                if (!isset($_POST[ "field_{$field["parameter"]->param_campo}" ]))
                {
                    $param_name = $field[ "parameter" ]->param_tp_nome;
                    if ( $param_name == "toggle_status" ) {
                        $_POST[ "field_{$field["parameter"]->param_campo}" ] = "0";
                    } else if ( $param_name == "ordem") {
                        // pegar ordem maior e incrementar
                        $maxOrder = $this->model->getMaxOrder( $field["parameter"]->param_campo );
                        $_POST[ "field_{$field["parameter"]->param_campo}" ] = $maxOrder+1;
                    } else {
                        continue;
                    }

                    // $error = true;
                    // break;
                }

                if ( is_null( $field["parameter"]->param_tp_nome ) ) {
                    $parameterName = "default";
                } else {
                    $parameterName = $field["parameter"]->param_tp_nome;
                }

                $methodName = "store";
                if ($view_type == "edita") {
                    $methodName = "update";
                }

                // Verificando existência da classe do parâmetro
                $className = "App\Controllers\Parameter\\" . Util::titleCase( $parameterName ) . "Parameter";
                if ( !class_exists( $className ) ) { continue; }
                
                $parameterInstance = new $className;

                $parameterInstance->args = array(
                    "field"     => $field,
                    "index"     => $i,
                    "value"     => $_POST[ "field_{$field["parameter"]->param_campo}" ],
                    "module"    => $this->model->table,
                    "params"    => $params,
                    "method"    => $methodName,
                    "primary_key" => $this->model->primary_key
                );
                
                $value = $parameterInstance->$methodName();
                if ($value === false) continue;

                $values[ $field["parameter"]->param_campo ] = $value;

                if ( count( $parameterInstance->errors ) > 0 ) {
                    $error = true;
                    $error_message = implode( "<br>", $parameterInstance->errors );
                    break;
                }

                $i++;
            }
        }

        $model = $this->model;
        $app = $this->app;
        // Sucesso
        $redirectWithSuccess = function() use ($model, $app, $args, $list) {
            $route = "/module/{$model->id_tabela}";

            if ( $_POST[ "after_action" ] == "form" ) {
                $route = "/module/{$model->id_tabela}/{$args["rest_path"]}";
            } else if ($list) {
                $route = "http://{$_SERVER[ "SERVER_NAME" ]}{$_POST[ "redirect" ]}";
            }
            
            $app->set("SESSION.after_action", $_POST[ "after_action" ]);
            $app->reroute( $route );
        };

        // Erro
        $redirectWithError = function() use ($app) {
            // header("Location: {$_SERVER['HTTP_REFERER']}");
            $app->reroute( $_SERVER['HTTP_REFERER'] );
        };

        if (!$error)
        {
            if ($view_type == "insere") {
                $this->model->create( $values );
            } else {
                $id_record = (int) $params[ "id_record" ];
                $this->model->update( $id_record, $values );
            }

            if ($this->model->error) {
                $this->app->set("SESSION.error", "Erro ao {$args["name_action"]} registro! {$this->model->error_message}");
                $redirectWithError();
            } else {
                $this->app->set("SESSION.success", "Registro {$args["past_action"]} com sucesso!");
                $redirectWithSuccess();
            }
        } else {
            // Mostrar erros
            $this->app->set("SESSION.error", $error_message);
            $redirectWithError();
        }
    }
}
