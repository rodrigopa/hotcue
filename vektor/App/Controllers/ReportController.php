<?php
namespace App\Controllers;
use App\Models\Report as Report;

class ReportController extends BaseController {

	/**
	 * Model
	 */
	protected $model = null;

	public function __construct()
	{
		parent::__construct();
		$this->model = new Report;
	}

	public function index()
	{
		echo $this->twig->render("report/index.html",
			array(
				"reports" => $this->model->getAll()
			)
		);
	}
	public function create()  	{}
	public function store()   	{}
	public function show()    	{}
	public function edit()    	{}
	public function update()  	{}
	public function destroy() 	{}
}
