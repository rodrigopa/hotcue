<?php
namespace App\Controllers;

class SessionController {
	private $app;
	public $user;

	public function afterRoute()
	{
		// Limpando as flash messages
		$this->app->set("SESSION.error", null);
	}

	public function __construct($app)
	{
		global $twig;
		$this->twig = &$twig;
		$this->app = \Base::instance();
	}

	public function index()
	{
		// Se estiver logado, vai para a página de avisos, se não
		// vai para a página de login
		if ($this->isLogged())
		{
			$this->app->reroute("/reports");
		}
		else
		{
			$this->app->reroute("/login");
		}
	}

	/**
	 * Formulário de login
	 * 
	 * @return none
	 */
	public function create()
	{
		// Se o usuário já estiver logado vai para a página de avisos
		if ($this->isLogged())
		{
			$this->app->reroute("/reports");
		}
		else
		{
			echo $this->twig->render("session/login.html");
		}
	}

	/**
	 * Tentar logar
	 * 
	 * @return none
	 */
	public function store()
	{
		$param = $this->app["POST"];
		if (isset($param["username"]) && isset($param["password"]))
		{
			$user = \ORM::for_table($this->app->get('vektor')["prefix"] . "users")
				->where_equal("username", $param["username"])
				->find_one();

			if ($user !== false && $user->password == $param["password"])
			{
				// Criando a sessão
				$_SESSION[$this->app->get('vektor')['session_name']] = array( "id" => $user->id, "password" => $user->password );
				// Redirecionando para a página de avisos
				$this->app->reroute("/reports");
			}
			else
			{
				$msg = null;

				if ($user === false)
					$msg = "Este usuário não existe!";
				else if ($user->password !== $param["password"])
					$msg = "A senha está incorreta!";
				
				$this->app->set("SESSION.error", $msg);
				$this->app->reroute("/login");
			}
		}
		else
		{
			$this->app->set("SESSION.error", "Você deixou algum campo em branco!");
			$this->app->reroute("/login");
		}
	}

	/**
	 * Destruir sessão
	 * 
	 * @return none
	 */
	public function destroy()
	{
		if ($this->isLogged())
		{
			unset($_SESSION[$this->app->get('vektor')['session_name']]);
		}

		$this->app->reroute('/login');
	}

	/**
	 * Saber se o usuário está logado
	 * 
	 * @return boolean Usuário está logado ou não
	 */
	public function isLogged()
	{
		$session_name = $this->app->get('vektor')['session_name'];
		// Verificando se sessão está ativa
		if (isset($_SESSION[$session_name]) && !empty($_SESSION[$session_name]))
		{
			$session = $_SESSION[$session_name];
			// Pegar usuário com base nos dados da sessão
			// por questões de segurança
			// 
			$user = \ORM::for_table($this->app->get('vektor')["prefix"] . "users")
				->where("id",       $session["id"])
				->where("password", $session["password"])
				->find_one();

			if ($user !== false)
			{
				$this->user = $user;
				return true;
			}

			return false;
		}

		return false;
	}
}
