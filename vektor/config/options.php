<?php
/**
 * Configurações gerais do Vektor
 *
 * @author Rodrigo Pinheiro <rodrigo.pinheiroa@hotmail.com>
 */

$site_options = require( "../config/options.php" );

return [
	// 
	// Nome do sistema
	// 
	// OBS: Não troque este nome se você não tiver as devidas permissões
	// de uso.
	// 
	'title' => 'HotCue Admin',
	'version' => "1.0",
	'base' => '/hotcue.com.br/www/vektor',
	// Dados do responsável dessa instância do sistema
	'admin' => [
		// Nome do responsável
		'name' => 'Rodrigo Pinheiro',
		// Email do responsável
		'email' => 'rodrigo.pinheiroa@hotmail.com'
	],
	'prefix' => 'vektor_',
	'session_name' => 'vektor.session',
	'db' => $site_options[ "db" ],
 	'max_size_delete' 	=> 20,
	'foreign_id_admin' 	=> 'admin_id',
	'placeholder_empty' => '----',

	// Uploads & Security
	'root'				=> __DIR__ . '/..',
	'upload_dir'		=> '/../public/upload',
	'allow_type_files' 	=> null, // null para aceitar todos os tipos
	'allow_type_images' => ["png", "jpg", "jpeg", "bmp"] // null para aceitar todos os tipos
];