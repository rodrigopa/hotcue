<?php
/**
 * Arquivo de configuração das rotas
 *
 * @author Rodrigo Pinheiro
 */

// Session routes
$app->route("GET /", "App\Controllers\SessionController->index");
$app->route("GET /login", "App\Controllers\SessionController->create");
$app->route("POST /login", "App\Controllers\SessionController->store");
$app->route("GET /logout", "App\Controllers\SessionController->destroy");

// Account routes
$app->route("GET /account", "App\Controllers\AccountController->index");
$app->route("PUT /account", "App\Controllers\AccountController->update");

// Report
$app->route("GET /reports", "App\Controllers\ReportController->index");

// Módulos genéricos
$app->route("GET /module/@id_module",                   "App\Controllers\ModuleController->index");
$app->route("GET /module/@id_module/popup",             "App\Controllers\ModuleController->popup");
$app->route("GET /module/@id_module/popup/page/@page",  "App\Controllers\ModuleController->popup");
$app->route("POST /module/@id_module/search",           "App\Controllers\ModuleController->postSearch");
// $app->route("GET /module/@id_module/print",            	"App\Controllers\ModuleController->printView");
$app->route("GET /module/@id_module/page/@page",        "App\Controllers\ModuleController->index");
$app->route("GET /module/@id_module/create",            "App\Controllers\ModuleController->create");
$app->route("POST /module/@id_module",                  "App\Controllers\ModuleController->store");
$app->route("GET /module/@id_module/@id_record",        "App\Controllers\ModuleController->show");
$app->route("GET /module/@id_module/@id_record/edit",   "App\Controllers\ModuleController->edit");
$app->route("PUT|PATCH /module/@id_module/@id_record",  "App\Controllers\ModuleController->update");
$app->route("DELETE /module/@id_module",     			"App\Controllers\ModuleController->destroy");

// Parâmetros
$app->route("GET /parameter/icon",           			"App\Controllers\Parameter\IconController->popup");

// Busca no painel
$app->route("POST /search", "App\Controllers\VektorController->search");

// // Tratamento de erros HTTP
// $app->set('ONERROR', function($app)
// {
//  // Resolvendo erro 404
//  if (isset($app["ERROR"]["code"]))
//  {
//      switch ($app["ERROR"]["code"])
//      {
//          case 404:
//              $app->reroute("/public/404.php");
//              break;

//      }
//  }
// });
