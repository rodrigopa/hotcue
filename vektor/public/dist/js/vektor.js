/* Chosen Select
   ========================================================================== */
$(".chosen-select").chosen({ no_results_text: "Nenhum resultado!" }); 

/* iCheck
   ========================================================================== */
$('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
	checkboxClass: 'icheckbox_minimal-red',
	radioClass: 'iradio_minimal-red'
});

/* Marcar exclusão
   ========================================================================== */

$( ".table-module" ).find( ".check-all" ).on( 'ifChecked', function( event ) {
  	$( ".table-module" ).find( ".check-record, .check-all" ).iCheck('check');
});

$( ".table-module" ).find( ".check-all" ).on( 'ifUnchecked', function( event ) {
  	$( ".table-module" ).find( ".check-record, .check-all" ).iCheck('uncheck');
});

/* Criação do formulário para exclusão
   ========================================================================== */

var deleteAll = function( path ) {
	var checkeds = $( ".table-module" ).find( ".check-record:checked" ),
		html_inputs;
	
	if (checkeds.length > 0) {
		if ( !confirm( "Tem certeza de que deseja apagar este(s) " + checkeds.length + " registro(s)?" ) ) return false;
		html_inputs = '<input type="hidden" name="_method" value="DELETE">';
		
		checkeds.each( function( k, v ) {
			v = $( v );
			html_inputs += '<input type="hidden" name="' + v.attr( "name" ) + '" value="' + v.val() + '">';
		} );

		$( '<form/>' , { html: html_inputs, method: "POST", action: path }).submit();
	}
}

/* ========================================================================== 
   PARAMETROS
   ========================================================================== */

/* Chave primária
   ========================================================================== */

var foreignKeyParameter = {

	openPopup: function( path, id_parent, field_id, field_view ) {
		var parentWindow = window.open(path + "/popup", "parent_foreign_popup", "width=600, height=500");
		window.foreignFieldId = field_id;
		window.foreignFieldView = field_view;
	},

	clear: function( field_id, field_view ) {
		$( "#field_" + field_id + ", #foreign_field_" + field_id ).val( null );
	}

}

$( "#popup-foreign" ).find( ".table" ).find( ".foreign_key_record" ).on( 'ifChecked', function( event ) {
 	var id_foreign = this.value;
 	var field_foreign = $( this ).parents( "tr" ).first().find( "td.foreign_field_" + window.opener.foreignFieldView ).text();
 	
 	$( window.opener.document.body ).find( "form" ).find( "#field_" + window.opener.foreignFieldId).val( id_foreign );
 	$( window.opener.document.body ).find( "form" ).find( "#foreign_field_" + window.opener.foreignFieldId).val( field_foreign );
 	window.close();
} );

/* Ícone
   ========================================================================== */

var iconParameter = {

	openPopup: function( path, id_parent, field_id, field_view ) {
		var parentWindow = window.open(path, "parent_icon_popup", "width=600, height=500");
		parentWindow.foreignFieldId = field_id;
	},

	clear: function( field_id, field_view ) {
		$( "#field_" + field_id ).val( null );
	}

}

$( "#popup-icon" ).find( "input[name=selected]" ).change( function( event ) {
 	var id_icon = this.value;
 	
 	$( window.opener.document.body ).find( "form" ).find( "#field_" + window.foreignFieldId).val( id_icon );
 	window.close();
} );


/* Máscaras
   ========================================================================== */

function mascara(o, f) {
	setTimeout(function() {
		o.value = f(o.value);
	}, 1);
}

function cpf(v) {
	v = v.replace( /\D/g , ""); //Remove tudo o que não é dígito
	v = v.replace( /(\d{3})(\d)/ , "$1.$2"); //Coloca um ponto entre o terceiro e o quarto dígitos
	v = v.replace( /(\d{3})(\d)/ , "$1.$2"); //Coloca um ponto entre o terceiro e o quarto dígitos
	//de novo (para o segundo bloco de números)
	v = v.replace( /(\d{3})(\d{1,2})$/ , "$1-$2"); //Coloca um hífen entre o terceiro e o quarto dígitos
	return v;
}

function cnpj(v) {
	v = v.replace( /\D/g , ""); //Remove tudo o que não é dígito
	v = v.replace( /^(\d{2})(\d)/ , "$1.$2"); //Coloca ponto entre o segundo e o terceiro dígitos
	v = v.replace( /^(\d{2})\.(\d{3})(\d)/ , "$1.$2.$3"); //Coloca ponto entre o quinto e o sexto dígitos
	v = v.replace( /\.(\d{3})(\d)/ , ".$1/$2"); //Coloca uma barra entre o oitavo e o nono dígitos
	v = v.replace( /(\d{4})(\d)/ , "$1-$2"); //Coloca um hífen depois do bloco de quatro dígitos
	return v;
}

function chama_mascara(o) {
	if (o.value.length > 14)
		mascara(o, cnpj);
	else
		mascara(o, cpf);
}

$(":input").inputmask();
$( ".mask-cpf-cnpj" ).each(function() {
	$(this).keyup(function() {
		chama_mascara( this );
	});
});
$(".parameter-valor").maskMoney({
	symbol    	:'', 
	showSymbol	: true,
	thousands 	:'.',
	decimal   	:',',
	symbolStay	: true
});

/* Datepickers
   ========================================================================== */
// data
$(".parameter-data").datepicker({
    format: "dd/mm/yyyy",
    autoclose: true,
    todayBtn: "linked",
    language: "pt-BR"
});

// data
$(".parameter-data_hora").datetimepicker({
    format: "dd/mm/yyyy - HH:ii",
    autoclose: true,
    todayBtn: true,
    language: "pt-BR"
});

$(".parameter-cor").colorpicker();

/* CKEditor
   ========================================================================== */

if ( $( ".parameter-html").length > 0 ) {
	$( ".parameter-html" ).each(function() {
		CKEDITOR.replace( $(this).attr( "id" ) );
	});
}

/* Boxer
   ========================================================================== */

$('.boxer').boxer();

/* Parameter: ordem
   ========================================================================== */

$( ".ordem-down" ).click( function() {
	var id_atual = $( this ).parents( "form" ).first().find( "input[name=id_record]" ).val();
	var id_novo  = $( this ).parents( "tr" ).first().next( "tr" ).find( "input[name=id_record]" ).val();

	var form_atual = $( this ).parents( "form" ).first();
	var input_atual = $( "<input>" ).attr( {
		type: "hidden",
		value: id_novo,
		name: "id_swap"
	} ).appendTo( form_atual );

	form_atual.submit();
});

$( ".ordem-up" ).click( function() {
	var id_atual = $( this ).parents( "form" ).first().find( "input[name=id_record]" ).val();
	var id_novo  = $( this ).parents( "tr" ).first().prev( "tr" ).find( "input[name=id_record]" ).val();

	var form_atual = $( this ).parents( "form" ).first();
	var input_atual = $( "<input>" ).attr( {
		type: "hidden",
		value: id_novo,
		name: "id_swap"
	} ).appendTo( form_atual );
	
	form_atual.submit();
});

/* Parameter: toggle_status
   ========================================================================== */

$('input[type="checkbox"]').not('.toggle_status_input').iCheck({
	checkboxClass: 'icheckbox_flat-blue',
	radioClass: 'iradio_flat-blue'
});

var elems = Array.prototype.slice.call(document.querySelectorAll('.toggle_status_input'));

elems.forEach(function(html) {
  	var switchery = new Switchery(html);

  	html.onchange = function() {
  		$( html ).parents( "form" ).first().submit();
	};
});

