<!DOCTYPE html>
<html <?php language_attributes(); ?> ng-app="app">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Hotcue.com.br</title>
    <base href="/hotcue.com.br/www/" />
    <?php wp_head(); ?>
</head>
<body>
    <header id="header" class="navbar">
        <div class="navbar-header">
            <div class="container">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a ui-sref="home" class="navbar-brand"><img src="<?php echo get_template_directory_uri();?>/assets/images/logo.png" alt="HotCue"></a>
            </div>
        </div>
        <div class="collapse navbar-collapse">
            <div class="container">
                <nav>
                    <ul class="nav navbar-nav menu">
                        <li><a ui-sref="home">A hotcue</a></li>
                        <li><a ui-sref="agenda">Agenda</a></li>
                        <li><a ui-sref="artistas">Artistas nacionais</a></li>
                        <li><a href="agenda.html">Artistas internacionais</a></li>
                        <li><a href="agenda.html">Equipe</a></li>
                        <li><a href="agenda.html">Hotnews</a></li>
                        <li><a href="agenda.html">Contato</a></li>
                    </ul>
                </nav>
                <nav>
                    <ul class="nav navbar-nav social">
                        <li><a href="<?php echo $general_fields[ "f_google" ];?>" target="_blank"><i class="fa fa-google-plus"></i></a></li>
                        <li><a href="http://www.twitter.com/<?php echo $general_fields[ "f_twitter" ];?>" target="_blank"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="<?php echo $general_fields[ "f_facebook" ];?>" target="_blank"><i class="fa fa-facebook"></i></a></li>
                    </ul>
                </nav>
            </div>
        </div>
    </header>

    <main class="main">
        <section id="banner">
            <div class="cycle-slideshow" data-cycle-fx="fade" data-cycle-timeout="0" data-cycle-slides="> a" data-cycle-prev="#banner .arrow-banner.left" data-cycle-next="#banner .arrow-banner.right">
                <a href="javascript:;"><img src="<?php echo get_template_directory_uri();?>/assets/images/banner/1.jpg" alt="Banner 1"></a>
                <a href="javascript:;"><img src="<?php echo get_template_directory_uri();?>/assets/images/banner/1.jpg" alt="Banner 2"></a>
                <a href="javascript:;"><img src="<?php echo get_template_directory_uri();?>/assets/images/banner/1.jpg" alt="Banner 3"></a>
            </div>
            <!-- /.cycle-slideshow -->

            <div class="container paging">
                <a href="javascript:;" class="arrow-banner left"><img src="<?php echo get_template_directory_uri();?>/assets/images/arrow-left-banner.png"></a></a>
                <a href="javascript:;" class="arrow-banner right"><img src="<?php echo get_template_directory_uri();?>/assets/images/arrow-right-banner.png"></a></a>
            </div>
        </section>
        <!-- /#banner -->

        <div id="site-content" class="home" ui-view name="content">
            
        </div>
    </main>

    <footer id="footer">
        <section class="newsletter">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4"><h1>Newsletter</h1></div>
                    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                        <form method="post" action="" id="form-newsletter">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12 col-lg-4">
                                    <input type="text" id="nome-newsletter" placeholder="Seu nome">
                                </div>
                                <div class="col-xs-12 col-sm-5 col-md-8 col-lg-5">
                                    <input type="email" id="nome-newsletter" placeholder="Seu e-mail">
                                </div>
                                <div class="col-xs-12 col-sm-3 col-md-4 col-lg-3">
                                    <button>Assinar <i class="fa fa-angle-right"></i></button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
        <section class="address">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-4">
                        <h3>Endereço</h3>
                        <p><?php echo $general_fields[ "f_address" ];?></p>
                    </div>
                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                        <h3>Telefones</h3>
                        <p><?php echo nl2br( $general_fields[ "f_phones" ] );?></p>
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        <h3>Email</h3>
                        <p><?php echo $admin_email;?></p>
                    </div>
                </div>
            </div>
        </section>
        <section class="signature">
            <div class="container">
                <span class="pull-left">© 2016 <strong>HOT CUE Booking Agency</strong> - Todos direitos reservados.</span>
                <span class="pull-right">
                    Design by <a href="" target="_blank">Tarcísio Novais</a>.<br>
                    Desenvolvido por <a href="http://www.harpiadev.com.br" target="_blank">Harpia Desenvolvimento</a>.
                </span>
            </div>
        </section>
    </footer>

    <!-- scripts -->
    <?php wp_footer(); ?>
    <div id="fb-root"></div>
    <script>(function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.5";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
</body>
</html>