<?php
/**
 * Arquivo de configuração das rotas
 *
 * @author Rodrigo Pinheiro
 */

//
// API
//
// news 
$app->route("GET /api/news", "App\Controllers\NewsController->index");
$app->route("GET /api/news/page/@page", "App\Controllers\NewsController->index");
$app->route("GET /api/news/@id", "App\Controllers\NewsController->show");

// agenda
$app->route("GET /api/agenda", "App\Controllers\AgendaController->index");
$app->route("GET /api/agenda/show", "App\Controllers\AgendaController->show");

// artist
$app->route("GET /api/artists/type", "App\Controllers\ArtistController->index");
$app->route("GET /api/artists/type/@type", "App\Controllers\ArtistController->index");
$app->route("GET /api/artists/@id", "App\Controllers\ArtistController->show");

// members
$app->route("GET /api/members", "App\Controllers\VektorUserController->index");

// contato
$app->route("POST /api/contact", "App\Controllers\ContactController->send");

// newsletter
$app->route("POST /api/newsletter", "App\Controllers\NewsletterController->store");

// request
$app->route("POST /api/request", "App\Controllers\RequestController->store");

// home
$app->route("GET *", "App\Controllers\SiteController->index");


// // Tratamento de erros HTTP
// $app->set('ONERROR', function($app)
// {
//  // Resolvendo erro 404
//  if (isset($app["ERROR"]["code"]))
//  {
//      switch ($app["ERROR"]["code"])
//      {
//          case 404:
//              $app->reroute("/public/404.php");
//              break;

//      }
//  }
// });
