<?php
/**
 * Configurações gerais
 *
 * @author Rodrigo Pinheiro <rodrigo.pinheiroa@hotmail.com>
 */

return [
	// 
	// Nome do sistema
	// 
	// OBS: Não troque este nome se você não tiver as devidas permissões
	// de uso.
	// 
	'title' => 'HotCue',
	'version' => "2.0",
	'base' => '/hotcue.com.br/www',
	// Dados do responsável dessa instância do sistema
	'admin' => [
		// Nome do responsável
		'name' => 'Rodrigo Pinheiro',
		// Email do responsável
		'email' => 'rodrigo.pinheiroa@hotmail.com'
	],
	'db' => [
		'connection_string' => 'mysql:host=localhost;dbname=hotcue;charset=utf8',
    	'username' => 'root',
    	'password' => 'vertrigo'
	]
];