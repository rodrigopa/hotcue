<?php
namespace App\Controllers;

use App\Models\News as News;

class NewsController extends BaseController {

	public function index() {
		if ( isset( $this->app->PARAMS[ "page" ] ) && is_numeric( $this->app->PARAMS[ "page" ] ) ) {
			$page = (int) $this->app->PARAMS[ "page" ];
		}

		$data = News::getMany( $page );
		$this->json( $data );
	}

	public function show() {
		$id = $this->app->PARAMS[ "id" ];
		$single = News::getOne( $id );

		if (!$single) {
			$this->json( array("error" => true, "code" => 404) );
		} else {
			$this->json( $single );
		}
	}

}
