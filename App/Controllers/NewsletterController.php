<?php
namespace App\Controllers;

use App\Models\Newsletter as Newsletter;

class NewsletterController extends BaseController {

	public function store() {
		$result = array();

		if ( isset( $_POST[ "name" ] ) && isset( $_POST[ "email" ] ) ) {
			try {
				$create = Newsletter::create();
				$create->name = $_POST[ "name" ];
				$create->email = $_POST[ "email" ];
				$create->save();
				$result = array( "error" => false );				
			} catch (\PDOException $e) {
				$result = array( "error" => true, "message" => $e );
			}
		} else {
			$result = array( "error" => true, "message" => "Você deixou algum campo em branco!" );
		}

		$this->json( $result );
	}

}
