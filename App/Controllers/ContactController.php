<?php
namespace App\Controllers;

class ContactController extends BaseController {

	const DEST_EMAIL 	= "contato.hotcue@hotmail.com";
	const DEST_NAME 	= "Contato HotCue";
	const DEST_SUBJECT 	= "Contato através do site";

	public function send() {
		$base = "http://{$_SERVER["SERVER_NAME"]}{$GLOBALS["options"]["base"]}";

		$vars = array(
			"base" 		=> $base,
			"fields" 	=> $_POST
		);

		// To send HTML mail, the Content-type header must be set
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";

		// Additional headers
		$headers .= "To: " . self::DEST_NAME . " <" . self::DEST_EMAIL . ">\r\n";
		$headers .= "From: {$_POST["nome"]} <{$_POST["email"]}>\r\n";

		// send json
		$this->json( array( "error" => !@mail(self::DEST_EMAIL, self::DEST_SUBJECT, $this->mailContent( $vars ), $headers)) );
	}

	public function mailContent($data) {
		ob_start();
		echo $this->twig->render("mail/contato.html", $data);
		return ob_get_clean();
	}

}
