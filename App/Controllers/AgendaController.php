<?php
namespace App\Controllers;

use App\Models\Agenda as Agenda;
use App\Models\Artist as Artist;

class AgendaController extends BaseController {

	public function index() {
		$months  	= Agenda::getMonths();
		$artists 	= Artist::getAllArtistsNames();
		$dates   	= Agenda::getMany();

		$data = array(
			"artists" 	=> $artists,
			"months"  	=> $months,
			"dates"   	=> $dates[ "rows" ]
		);

		$this->json( $data );
	}

	public function show() {
		$month = null;
		$artist = null;

		if (isset( $_GET[ "month" ] ) && is_numeric( $_GET[ "month" ] )) {
			$month = $_GET[ "month" ];
		}

		if (isset( $_GET[ "artist" ] ) && is_numeric( $_GET[ "artist" ] )) {
			$artist = $_GET[ "artist" ];
		}

		$data = Agenda::getMany( $month, $artist );
		$this->json( $data );
	}

}
