<?php
namespace App\Controllers;

use App\Models\Banner as Banner;

class SiteController extends BaseController {

	public function index() {
		$vars = array( "banners" => Banner::getMany() );
		echo $this->twig->render( "home/index.html", $vars );
	}

}
