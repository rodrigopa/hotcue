<?php
namespace App\Controllers;

class Util {

	public static function slugify($text)
	{
		// replace non letter or digits by -
		$text = preg_replace('~[^\\pL\d]+~u', '-', $text);

		// trim
		$text = trim($text, '-');

		// transliterate
		$text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

		// lowercase
		$text = strtolower($text);

		// remove unwanted characters
		$text = preg_replace('~[^-\w]+~', '', $text);

		if (empty($text))
		{
			return 'n-a';
		}

		return $text;
	}

	public static function pastTime($time)
	{
		if ( !is_numeric($time)) $time = strtotime( $time );
		$diff = time() - $time;
		$segundos = $diff;
		$minutos = round($diff / 60);
		$horas = round($diff / 3600);
		$dias = round($diff / 86400);
		$semanas = round($diff / 604800);
		$meses = round($diff / 2419200);
		$años = round($diff / 29030400);
		
		if($segundos < 10) return "justo ahora";
		if($segundos < 60) return "hace ".$segundos." segundos";
		else if($minutos < 60) return $minutos==1 ?"hace 1 minuto": "hace " .$minutos." minutos";
		else if($horas < 24) return $horas==1 ?"hace 1 hora": "hace " .$horas." horas";
		else if($dias < 7) return $dias==1 ?"hace 1 dia": "hace " .$dias." dias";
		else if($semanas < 4) return $semanas==1 ?"hace 1 semana": "hace " .$semanas." semanas";
		else if($meses < 12) return $meses == 1 ?"hace 1 mes": "hace " .$meses." meses";
		else return $años == 1 ? "hace 1 año": "hace " .$años." años";
	}

	public static function limitString($cadena, $limite, $separador=" ", $fin="...")
	{
		$cadena = strip_tags($cadena);
		if(strlen($cadena) <= $limite) 
			return $cadena;
		if(($breakpoint = strpos($cadena, $separador, $limite)) !== false){
			if($breakpoint < strlen($cadena)-1) { 
				$cadena = substr($cadena, 0, $breakpoint) . $fin;
			}
		}
		return $cadena;
	}

	public static function getStringMonth($mes)
	{
		$mes = (int) $mes;
		$months = array("Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro");
		return $months[ $mes-1 ];
	}

	public static function HTMLSelect(Array $options, $value = null, $name = null, $placeholder = false, $required = false) {
		$html = "<select";
		if (!is_null($name)) $html .= " name=\"{$name}\"";
		if ($required) $html .= " required";
		$html .= ">";

		if ($placeholder) $html .= "<option>Selecione...</option>";
		foreach ($options as $key => $option) {
			$html .= "<option value=\"{$key}\"" . ( $key == $value ? " selected" : "" ) . ">{$option}</option>";
		}

		$html .= "</select>";

		return $html;
	}

	public static function get_useragent_info($ua = null)
	{
	    $ua = is_null($ua) ? $_SERVER['HTTP_USER_AGENT'] : $ua;
	    // Enumerate all common platforms, this is usually placed in braces (order is important! First come first serve..)
	    $platforms = "Windows|iPad|iPhone|Macintosh|Android|BlackBerry";

	    // All browsers except MSIE/Trident and.. 
	    // NOT for browsers that use this syntax: Version/0.xx Browsername  
	    $browsers = "Firefox|Chrome"; 

	    // Specifically for browsers that use this syntax: Version/0.xx Browername  
	    $browsers_v = "Safari|Mobile"; // Mobile is mentioned in Android and BlackBerry UA's

	    // Fill in your most common engines..
	    $engines = "Gecko|Trident|Webkit|Presto";

	    // Regex the crap out of the user agent, making multiple selections and.. 
	    $regex_pat = "/((Mozilla)\/[\d\.]+|(Opera)\/[\d\.]+)\s\(.*?((MSIE)\s([\d\.]+).*?(Windows)|({$platforms})).*?\s.*?({$engines})[\/\s]+[\d\.]+(\;\srv\:([\d\.]+)|.*?).*?(Version[\/\s]([\d\.]+)(.*?({$browsers_v})|$)|(({$browsers})[\/\s]+([\d\.]+))|$).*/i";

	    // .. placing them in this order, delimited by |
	    $replace_pat = '$7$8|$2$3|$9|${17}${15}$5$3|${18}${13}$6${11}';

	    // Run the preg_replace .. and explode on |
	    $ua_array = explode("|",preg_replace($regex_pat, $replace_pat, $ua, PREG_PATTERN_ORDER));

	    if (count($ua_array)>1)
	    {
	        $return['platform']  = $ua_array[0];  // Windows / iPad / MacOS / BlackBerry
	        $return['type']      = $ua_array[1];  // Mozilla / Opera etc.
	        $return['renderer']  = $ua_array[2];  // WebKit / Presto / Trident / Gecko etc.
	        $return['browser']   = $ua_array[3];  // Chrome / Safari / MSIE / Firefox

	        /* 
	           Not necessary but this will filter out Chromes ridiculously long version
	           numbers 31.0.1234.122 becomes 31.0, while a "normal" 3 digit version number 
	           like 10.2.1 would stay 10.2.1, 11.0 stays 11.0. Non-match stays what it is.
	        */

	        if (preg_match("/^[\d]+\.[\d]+(?:\.[\d]{0,2}$)?/",$ua_array[4],$matches))
	        {
	            $return['version'] = $matches[0];     
	        }
	        else
	        {
	            $return['version'] = $ua_array[4];
	        }
	    }
	    else
	    {
	        /*
	           Unknown browser.. 
	           This could be a deal breaker for you but I use this to actually keep old
	           browsers out of my application, users are told to download a compatible
	           browser (99% of modern browsers are compatible. You can also ignore my error
	           but then there is no guarantee that the application will work and thus
	           no need to report debugging data.
	         */

	        return false;
	    }

	    // Replace some browsernames e.g. MSIE -> Internet Explorer
	    switch(strtolower($return['browser']))
	    {
	        case "msie":
	        case "trident":
	            $return['browser'] = "Internet Explorer";
	            break;
	        case "": // IE 11 is a steamy turd (thanks Microsoft...)
	            if (strtolower($return['renderer']) == "trident")
	            {
	                $return['browser'] = "Internet Explorer";
	            }
	        break;
	    }

	    switch(strtolower($return['platform']))
	    {
	        case "android":    // These browsers claim to be Safari but are BB Mobile 
	        case "blackberry": // and Android Mobile
	            if ($return['browser'] =="Safari" || $return['browser'] == "Mobile" || $return['browser'] == "")
	            {
	                $return['browser'] = "{$return['platform']} mobile";
	            }
	            break;
	    }

	    return $return;
	}

	public static function curl_request($url) {
		$ch = curl_init();

		$curl_options = array(
            CURLOPT_URL 			=> $url,
            CURLOPT_RETURNTRANSFER 	=> true,
            CURLOPT_FOLLOWLOCATION 	=> true,
        );

		curl_setopt_array( $ch, $curl_options );

        $response = curl_exec($ch);
        curl_close($ch);

        return $response;
	}

	public static function get_ip_info() {
		$ip             	= $_SERVER["REMOTE_ADDR"];
		$url            	= "http://freegeoip.net/json/" . ( $ip == "127.0.0.1" || $ip == "::1" ? "" : "{$ip}" );
		$ip_info        	= json_decode( self::curl_request( $url ) );
		$q              	= str_replace( ',', '.', $ip_info->latitude ) . "," . str_replace( ',', '.', $ip_info->longitude );
		$results_google 	= json_decode( self::curl_request( "http://maps.googleapis.com/maps/api/geocode/json?latlng={$q}&sensor=true" ) );

		$data = array();
		$data[ "latitude" ] = $ip_info->latitude;
		$data[ "longitude" ] = $ip_info->longitude;
		
		foreach ( $results_google->results as $result ) {
			if ( in_array("political", $result->types) ) {
				foreach ( $result->address_components as $component ) {
					if ( in_array( "administrative_area_level_2", $component->types ) ) {
						$data[ "city" ] = $component->long_name;
					} else if ( in_array( "administrative_area_level_1", $component->types ) ) {
						$data[ "region_name" ] = $component->long_name;
					} else if ( in_array( "country", $component->types ) ) {
						$data[ "country_name" ] = $component->long_name;
						$data[ "country_code" ] = $component->short_name;
					}
				}

				break;
			}
		}

       	return json_decode( json_encode( $data ) );
	}

	public static function mySqlDate($timestamp = null) {
		return !is_null( $timestamp ) ? date("Y-m-d H:i:s", $timestamp) : date("Y-m-d H:i:s");
	}

	public static function bbcode($string) {
		$mensaje = nl2br($string);
		$bbc = array( 
			'/\[b\]([\s\S]*?)\[\/b\]/',
			'/\[i\]([\s\S]*?)\[\/i\]/',
			'/\[u\]([\s\S]*?)\[\/u\]/',
			'/\[s\]([\s\S]*?)\[\/s\]/',
			'/\[sub\]([\s\S]*?)\[\/sub\]/',
			'/\[left\]([\s\S]*?)\[\/left\]/',
			'/\[center\]([\s\S]*?)\[\/center\]/',
			'/\[right\]([\s\S]*?)\[\/right\]/',
			'/\[justify\]([\s\S]*?)\[\/justify\]/',
			'/\[color=([\s\S]*?)\]([\s\S]*?)\[\/color\]/',
			'/\[ul\]([\s\S]*?)\[\/ul\]/',
			'/\[ol\]([\s\S]*?)\[\/ol\]/',
			'/\[li\]([\s\S]*?)\[\/li\]/',
			'/\[code\]([\s\S]*?)\[\/code\]/',
			'/\[quote\]([\s\S]*?)\[\/quote\]/',
			'/\[hr\]/',
			'/\[img\](.*?)\[\/img\]/',
			'/\[img=(.*?)x(.*?)\](.*?)\[\/img\]/',
			'/\[email\](.*?)\[\/email\]/',
			'/\[email=(.*?)\](.*?)\[\/email\]/',
			'/\[url\]([\s\S]*?)\[\/url\]/',
			'/\[url=([\s\S]*?)\](.*?)\[\/url\]/',
			'/\[youtube\](.*?)\[\/youtube\]/'
		); 

		$htmlc = array( 
			'<b>$1</b>',
			'<i>$1</i>',
			'<u>$1</u>',
			'<s>$1</s>',
			'<sub>$1</sub>',
			'<div align="left">$1</div>',
			'<div align="center">$1</div>',
			'<div align="right">$1</div>',
			'<div align="justify">$1</div>',
			'<font color="$1">$2</font>',
			'<ul>$1</ul>',
			'<ol>$1</ol>',
			'<li>$1</li>',
			'<div><code>$1</code></div>',
			'<div><blockquote>$1</blockquote></div>',
			'<hr>',
			'<img src="$1">',
			'<div style="background-image:url($3); width: $1px; height: $1px;" title="HabboSecurity" class="cinthya_light"></div>',
			'<a href="mailto:$1">$1</a>',
			'<a href="mailto:$1">$2</a>',
			'<a href="$1">$1</a>',
			'<a href="$1">$2</a>',
			'<iframe width="560" height="315" src="http://www.youtube.com/embed/$1?wmode=opaque" data-youtube-id="$1" frameborder="0" allowfullscreen=""></iframe>'
		); 

		$mensaje = preg_replace($bbc, $htmlc, $mensaje);
		return $mensaje;
	}

	public static function bbcode_message($string) {
		$mensaje = nl2br($string);
		$bbc = array( 
			'/\[b\]([\s\S]*?)\[\/b\]/',
			'/\[i\]([\s\S]*?)\[\/i\]/',
			'/\[u\]([\s\S]*?)\[\/u\]/',
			'/\[s\]([\s\S]*?)\[\/s\]/',
			'/(^|\s)@(\w+)/',
			'/(^|\s)#(\w+)/'
		); 

		$htmlc = array( 
			'<b>$1</b>',
			'<i>$1</i>',
			'<u>$1</u>',
			'<s>$1</s>',
			'\1<a target="_blank" href="http://www.twitter.com/\2">@\2</a>',
			'\1<a target="_blank" href="http://search.twitter.com/search?q=%23\2">#\2</a>'
		);
		
		$mensaje = preg_replace($bbc, $htmlc, $mensaje);
		return $mensaje;
	}
}
