<?php
namespace App\Controllers;

use App\Models\Agenda as Agenda;
use App\Models\Artist as Artist;

class ArtistController extends BaseController {

	public function index() {
		$type = null;

		if ( isset( $this->app->PARAMS[ "type" ] ) ) {
			$type = $this->app->PARAMS[ "type" ];
		}

		$artists = Artist::getMany($type);
		$this->json( $artists );
	}

	public function show() {
		$id = $this->app->PARAMS[ "id" ];
		$single = Artist::getOne( $id );

		if (!$single) {
			$this->json( array("error" => true, "code" => 404) );
		} else {
			$this->json( $single );
		}
	}

}
