<?php
namespace App\Controllers;

use App\Models\Request as Request;

class RequestController extends BaseController {

	public function store() {
		$result = array();

		if ( isset( $_POST[ "nome" ] ) &&
			isset( $_POST[ "email" ] ) && 
			isset( $_POST[ "telefone" ] ) &&
			isset( $_POST[ "localizacao" ] ) &&
			isset( $_POST[ "mensagem" ] ) &&
			isset( $_POST[ "artists" ] ) )
		{
			try {
				$create = Request::create();
				$create->name = $_POST[ "nome" ];
				$create->email = $_POST[ "email" ];
				$create->phone = $_POST[ "telefone" ];
				$create->location = $_POST[ "localizacao" ];
				$create->message = $_POST[ "mensagem" ];
				$create->artists = $_POST[ "artists" ];
				$create->ip = $_SERVER[ "REMOTE_ADDR" ];
				$create->date = time();
				$create->save();
				$result = array( "error" => false );				
			} catch (\PDOException $e) {
				$result = array( "error" => true, "message" => $e );
			}
		} else {
			$result = array( "error" => true, "message" => "Você deixou algum campo em branco!" );
		}

		$this->json( $result );
	}

}
