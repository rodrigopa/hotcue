<?php
namespace App\Controllers;

use App\Models\News as News;
use App\Models\Forum as Forum;
use App\Models\Page as Page;
use App\Models\SessionUser as SessionUser;
use App\Models\SiteOptions as SiteOptions;

class BaseController {
    /**
     * Instância do objeto da aplicação Fat Free
     * @var FatFree
     */
    protected $app;

    /**
     * TWIG
     */
    protected $twig;

    /**
     * site options
     */
    protected $siteOptions;

    // Rodar antes de cada requisição
    public function beforeRoute()
    {
        // get site options
        $siteOptionsObjects = SiteOptions::model()->find_many();
        $siteOptions = array();

        foreach ($siteOptionsObjects as $object) {
            $siteOptions[ $object->option_key ] = $object->option_value;
        }

        // add to twig template
        $this->twig->addGlobal( "site_options", $siteOptions );
    }

    // Rodar depois de cada requisição
    public function afterRoute()
    {
        $this->clearFlash();
    }

    public function clearFlash()
    {
        // Limpando as flash messages
        $this->app->set("SESSION.error", null);
        $this->app->set("SESSION.success", null);
    }

    /**
     * Método construtor
     * @param FatFree $app Instância do objeto da aplicação
     */
    public function __construct()
    {
        global $twig;
        $this->twig = &$twig;
        $this->app = \Base::instance();
    }

    protected function error() {
        header( "HTTP/1.0 404 Not Found" );
        echo $this->twig->render( "layout/404.html" );
        exit;
    }

    public function requireLogin() {
        if ( is_null( $this->user_session ) ) {
            $this->app->set( "SESSION.error", "Você precisa estar logado para acessar!" );
            $this->app->reroute( "/" );
        }
    }

    public function json($data)
    {
        header( "Content-Type: application/json" );
        echo json_encode( $data );
        exit;
    }
}
