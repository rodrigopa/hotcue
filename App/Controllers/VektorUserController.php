<?php
namespace App\Controllers;

use App\Models\VektorUser as VektorUser;

class VektorUserController extends BaseController {

	public function index() {
		$data = VektorUser::getMany();
		$this->json( $data );
	}

}
