<?php
namespace App\Models;

class VektorUser extends BaseModel {
	/**
	 * table name
	 */
	public $_table = "vektor_users";

	/**
	 * model factory
	 */
	public static function model($model = __CLASS__) {
		return \Model::factory( $model );
	}

	public static function getMany() {
		$object = self::model();
		$object = $object->order_by_desc("id")->find_many();
		$data = array();

		foreach ($object as $single) {
			$social = array();
			if ( !is_null( $single->twitter ) ) $social[ "twitter" ] = $single->twitter;
			if ( !is_null( $single->facebook ) ) $social[ "facebook" ] = $single->facebook;
			if ( !is_null( $single->instagram ) ) $social[ "instagram" ] = $single->instagram;
			if ( !is_null( $single->google_plus ) ) $social[ "google_plus" ] = $single->google_plus;
			if ( !is_null( $single->skype ) ) $social[ "skype" ] = $single->skype;

			$data[] = array(
				"name"        	=> $single->name,
				"role"        	=> $single->role,
				"avatar"       	=> $single->avatar,
				"social"		=> $social
			);
		}

		return $data;
	}
}
