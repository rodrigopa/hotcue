<?php
namespace App\Models;

use App\Controllers\Util as Util;

class News extends BaseModel {
	/**
	 * table name
	 */
	public $_table = "news";

	/**
	 * model factory
	 */
	public static function model($model = __CLASS__) {
		return \Model::factory( $model );
	}

	/**
	 * author relation
	 */
	public function user() {
		return $this->belongs_to( "App\Models\VektorUser", "admin_id" );
	}

	/**
	 * category relation
	 */
	public function category() {
		return $this->belongs_to( "App\Models\CategoryNews", "category_id" );
	}

	/**
	 * get news
	 */
	public static function getMany( $page = 1 ) {
		// News objects
		$news_all = self::model();
		$news = self::model();
		
		$per_page = 8;
		if ( $page == 1 ) $per_page = 5;

		$total_regs = $news_all->count();
		$total_pages = ceil( $total_regs / $per_page );

		if ( $page <= 0 || $page > $total_pages ) {
			$page = 1;
		}

		$offset = ( $page * $per_page ) - $per_page;
		$news = $news->order_by_desc( "id" );
		$news = $news
				->limit( $per_page )
				->offset( $offset )
				->find_many();

		// data array
		$data = array();
		$data[ "end" ] = ( $page == $total_pages );

		foreach ( $news as $i => $single ) {
			$user = $single->user()->find_one();
			$category = $single->category()->find_one();
			$new = ($page == 1 && $i == 0);

			$data[ "rows" ][] = array(
				"id"             	=> (int) $single->id,
				"author"         	=> $user->name,
				"title"          	=> $single->title,
				"description"    	=> $single->description,
				"category"       	=> array(
					"title" => $category->title,
					"color" => $category->color,
				),
				"image" 			=> $single->image,
				"url"            	=> Util::slugify( $single->title ),
				"new"				=> $new,
			);
		}

		return $data;
	}


	public static function getOne( $id ) {
		$single = News::model()->where( "id", $id )->find_one();

		if (!$single) {
			return false;
		} else {
			$user = $single->user()->find_one();
			$category = $single->category()->find_one();
			$lastNews = self::getLastNews( $single->id );

			return array(
				"id"             	=> (int) $single->id,
				"author"         	=> $user->name,
				"title"          	=> $single->title,
				"description"    	=> $single->description,
				"text"    			=> $single->text,
				"date"    			=> $single->date,
				"formatedDate"    	=> date("H:i d/m/Y", $single->date),
				"category"       	=> array(
					"title" => $category->title,
					"color" => $category->color,
				),
				"image" 			=> $single->image,
				"lastNews"			=> $lastNews
			);
		}
	}

	public static function getLastNews( $id_except ) {
		$object = News::model()->where_not_equal( "id", $id_except )->order_by_desc( "id" )->find_many();
		$data = array();

		foreach ($object as $item) {
			$category = $item->category()->find_one();

			$data[] = array(
				"id"             	=> (int) $item->id,
				"title"          	=> $item->title,
				"url"            	=> Util::slugify( $item->title ),
				"category"       	=> array(
					"title" => $category->title,
					"color" => $category->color,
				),
			);
		}

		return $data;
	}
	
}
