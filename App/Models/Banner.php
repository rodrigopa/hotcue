<?php
namespace App\Models;

class Banner extends BaseModel {
	
	/**
	 * table name
	 */
	public $_table = "banner";

	/**
	 * model factory
	 */
	public static function model($model = __CLASS__) {
		return \Model::factory( $model );
	}

	public static function getMany() {
		$object = Banner::model()->order_by_desc("id")->find_many();

		$data = array();
		foreach ($object as $item) {
			$data[] = $item->orm->_data;
		}

		return $data;
	}

}
