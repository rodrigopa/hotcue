<?php
namespace App\Models;

class Newsletter extends BaseModel {
	
	/**
	 * table name
	 */
	public $_table = "newsletter";

	/**
	 * model factory
	 */
	public static function model($model = __CLASS__) {
		return \Model::factory( $model );
	}

}
