<?php
namespace App\Models;

use App\Controllers\Util as Util;

class Artist extends BaseModel {
	/**
	 * table name
	 */
	public $_table = "artist";

	/**
	 * model factory
	 */
	public static function model($model = __CLASS__) {
		return \Model::factory( $model );
	}

	/**
	 * agenda relation
	 */
	public function agenda() {
		return $this->has_many( "App\Models\Agenda", "artist_id" );
	}

	/**
	 * pictures relation
	 */
	public function pictures() {
		return $this->has_many( "App\Models\ArtistPicture", "artist_id" );
	}

	public static function getMany($type) {
		$object = self::model();

		if (!is_null($type)) {
			$object = $object->where( "type", self::getTypeOptions ()[ $type ] );
		}

		$object = $object->order_by_desc("id")->find_many();
		$data = array();

		foreach ($object as $single) {
			$data[] = array(
				"id"			=> $single->id,
				"name"        	=> $single->name,
				"place"        	=> $single->place,
				"nameSlug"      => Util::slugify( $single->name ),
				"image"       	=> $single->image,
				"image_white" 	=> $single->image_white,
				"image_black" 	=> $single->image_black,
				"type"			=> strtolower( $single->type )
			);
		}

		return $data;
	}

	public static function getOne( $id ) {
		$single = Artist::model()->where( "id", $id )->find_one();

		if (!$single) {
			return false;
		} else {
			$picturesArray = array();
			$pictures = $single->pictures()->order_by_desc("id")->find_many();

			foreach ($pictures as $picture) {
				$picturesArray[] = $picture->orm->_data;
			}

			return $single->orm->_data + array( "pictures" => $picturesArray );
		}
	}

	public static function getAllArtistsNames() {
		$object = self::model();
		$object = $object
					->order_by_asc( "name" )
					->find_many();

		$result = array();
		foreach ($object as $single) {
			$result[] = array(
				"id"   	=> $single->id,
				"name" 	=> $single->name
			);
		}

		return $result;
	}

	// type consts
	const TYPE_NACIONAL = "nacionais";
	const TYPE_INTERNACIONAL = "internacionais";

	public static function getTypeOptions() {
		return array(
			self::TYPE_NACIONAL => "Nacional",
			self::TYPE_INTERNACIONAL => "Internacional"
		);
	}
}
