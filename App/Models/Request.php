<?php
namespace App\Models;

class Request extends BaseModel {
	
	/**
	 * table name
	 */
	public $_table = "request";

	/**
	 * model factory
	 */
	public static function model($model = __CLASS__) {
		return \Model::factory( $model );
	}

}
