<?php
namespace App\Models;

class SiteOptions extends BaseModel {
	
	/**
	 * table name
	 */
	public $_table = "options";

	/**
	 * model factory
	 */
	public static function model($model = __CLASS__) {
		return \Model::factory( $model );
	}

}
