<?php
namespace App\Models;

class ArtistPicture extends BaseModel {
	
	/**
	 * table name
	 */
	public $_table = "artist_picture";

	/**
	 * model factory
	 */
	public static function model($model = __CLASS__) {
		return \Model::factory( $model );
	}

}
