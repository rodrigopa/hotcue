<?php
namespace App\Models;

class BaseModel extends \Model {

	public static function model($model = __CLASS__) {
		return \Model::factory( $model );
	}

}