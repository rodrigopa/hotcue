<?php
namespace App\Models;

class CategoryNews extends BaseModel {
	
	/**
	 * table name
	 */
	public $_table = "news_category";

	/**
	 * model factory
	 */
	public static function model($model = __CLASS__) {
		return \Model::factory( $model );
	}

}
