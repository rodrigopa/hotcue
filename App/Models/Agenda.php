<?php
namespace App\Models;

use App\Controllers\Artist as Artist;
use App\Controllers\Util as Util;

class Agenda extends BaseModel {
	/**
	 * table name
	 */
	public $_table = "agenda";

	const SQL_MONTHS_DISTINCT = "SELECT MONTH(date) as month FROM agenda GROUP BY MONTH(date)";

	/**
	 * model factory
	 */
	public static function model($model = __CLASS__) {
		return \Model::factory( $model );
	}

	/**
	 * artist relation
	 */
	public function artist() {
		return $this->belongs_to( "App\Models\Artist", "artist_id" );
	}

	/**
	 * get agenda
	 */
	public static function getMany( $month = null, $artist = null ) {
		// News objects
		$agenda = self::model();

		$agenda = $agenda
					->where_raw( "YEAR(date) = ?", array( date( "Y" ) ) )
					->order_by_desc( "id" );


		if (is_null($month)) {
			$month = date( "n" );
		}

		$agenda = $agenda->where_raw( "MONTH(date) = ?", array( $month ) );

		if ( !is_null( $artist ) ) {
			$agenda = $agenda->where( "artist_id", $artist );
		}

		$agenda = $agenda
					->find_many();

		// data array
		$data = array();

		foreach ( $agenda as $i => $single ) {
			$artist = $single->artist()->find_one();
			$time = strtotime($single->date);
			$month = date( "n", $time );

			$data[ "rows" ][] = array(
				"id"             	=> (int) $single->id,
				"title"          	=> $single->title,
				"address"    		=> $single->address,
				"date"				=> array(
					"day"   	=> date( "j", $time ),
					"month" 	=> $month,
					"year"  	=> date( "Y", $time ),
					"monthStr" 	=> Util::getStringMonth( $month )
				),
				"artist" 			=> $artist->name,
				"link" 				=> $single->facebook_event
			);
		}

		return $data;
	}

	public static function getMonths() {
		$query = self::model()->raw_query( self::SQL_MONTHS_DISTINCT )->find_many();
		$result = array();

		foreach ($query as $item) {
			$result[] = array(
				"selected" => ( $item->month == date( "m" ) ),
				"id" 	=> (int) $item->month,
				"name" 	=> Util::getStringMonth($item->month)
			);
		}

		return $result;
	}
}
